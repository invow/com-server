import http from "http";
import https from "https";
import Express from "express";
import cors from "cors";
import { env, mongo, port, sport, ip, apiRoot } from "./config";
import mongoose from "./services/mongoose";
import express from "./services/express";
import api from "./api";
import fs from "fs";
import path from "path";

const app = express(apiRoot, api);
const server = http.createServer(app);

const staticsUrl = __dirname.replace("server", "client").replace("src", "dist");

//app.use(cors())

// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', '*')
//   res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method')
//   res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
//   res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE')
//   next()
// })

// Point static path to dist
app.use(Express.static(staticsUrl));

// app.get('*', (req, res) => {
//      res.sendFile(staticsUrl + '/index.html')
// })

mongoose.connect(mongo.uri, { useNewUrlParser: true });
mongoose.Promise = Promise;

setImmediate(() => {
  server.listen(port, ip, () => {
    console.log(
      "Express server listening on http://%s:%d, in %s mode",
      ip,
      port,
      env
    );
  });
  if (process.env.NODE_ENV === "production") {
    const keyPath = path.join(__dirname, "keys/private.key");
    const certPath = path.join(__dirname, path.join("keys/", process.env.CERT));
    const caPath = path.join(__dirname, "keys/intermediate.crt");

    const keys = {
      key: fs.readFileSync(keyPath),
      cert: fs.readFileSync(certPath),
      ca: fs.readFileSync(caPath),
    };

    const credentials = { key: keys.key, cert: keys.cert, ca: keys.ca };
    const secureServer = https.createServer(credentials, app);

    secureServer.listen(sport, ip, () => {
      console.log(
        "HTTPS Express server listening on https://%s:%d, in %s mode",
        ip,
        sport,
        env
      );
    });
  }
});

export default app;
