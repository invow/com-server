import { success, notFound } from '../../services/response/'
import { Favorites } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Favorites.create(body)
    .then((favorites) => favorites.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Favorites.find(query, select, cursor)
    .then((favorites) => favorites.map((favorites) => favorites.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Favorites.findById(params.id)
    .then(notFound(res))
    .then((favorites) => favorites ? favorites.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Favorites.findById(params.id)
    .then(notFound(res))
    .then((favorites) => favorites ? Object.assign(favorites, body).save() : null)
    .then((favorites) => favorites ? favorites.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Favorites.findById(params.id)
    .then(notFound(res))
    .then((favorites) => favorites ? favorites.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
