import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Favorites, { schema } from './model'

const router = new Router()
const { link, user } = schema.tree

/**
 * @api {post} /favorites Create favorites
 * @apiName CreateFavorites
 * @apiGroup Favorites
 * @apiParam link Favorites's link.
 * @apiParam user Favorites's user.
 * @apiSuccess {Object} favorites Favorites's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Favorites not found.
 */
router.post('/',
  body({ link, user }),
  create)

/**
 * @api {get} /favorites Retrieve favorites
 * @apiName RetrieveFavorites
 * @apiGroup Favorites
 * @apiUse listParams
 * @apiSuccess {Object[]} favorites List of favorites.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /favorites/:id Retrieve favorites
 * @apiName RetrieveFavorites
 * @apiGroup Favorites
 * @apiSuccess {Object} favorites Favorites's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Favorites not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /favorites/:id Update favorites
 * @apiName UpdateFavorites
 * @apiGroup Favorites
 * @apiParam link Favorites's link.
 * @apiParam user Favorites's user.
 * @apiSuccess {Object} favorites Favorites's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Favorites not found.
 */
router.put('/:id',
  body({ link, user }),
  update)

/**
 * @api {delete} /favorites/:id Delete favorites
 * @apiName DeleteFavorites
 * @apiGroup Favorites
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Favorites not found.
 */
router.delete('/:id',
  destroy)

export default router
