import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { About } from '.'

const app = () => express(apiRoot, routes)

let about

beforeEach(async () => {
  about = await About.create({})
})

test('POST /abouts 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ title: 'test', content: 'test', video: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.title).toEqual('test')
  expect(body.content).toEqual('test')
  expect(body.video).toEqual('test')
})

test('GET /abouts 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /abouts/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${about.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(about.id)
})

test('GET /abouts/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /abouts/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${about.id}`)
    .send({ title: 'test', content: 'test', video: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(about.id)
  expect(body.title).toEqual('test')
  expect(body.content).toEqual('test')
  expect(body.video).toEqual('test')
})

test('PUT /abouts/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ title: 'test', content: 'test', video: 'test' })
  expect(status).toBe(404)
})

test('DELETE /abouts/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${about.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /abouts/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${about.id}`)
  expect(status).toBe(401)
})

test('DELETE /abouts/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
