import { About } from '.'

let about

beforeEach(async () => {
  about = await About.create({ title: 'test', content: 'test', video: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = about.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(about.id)
    expect(view.title).toBe(about.title)
    expect(view.content).toBe(about.content)
    expect(view.video).toBe(about.video)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = about.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(about.id)
    expect(view.title).toBe(about.title)
    expect(view.content).toBe(about.content)
    expect(view.video).toBe(about.video)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
