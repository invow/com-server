import { success, notFound } from '../../services/response/'
import { About } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  About.create(body)
    .then((about) => about.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  About.find(query, select, cursor)
    .then((abouts) => abouts.map((about) => about.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  About.findById(params.id)
    .then(notFound(res))
    .then((about) => about ? about.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  About.findById(params.id)
    .then(notFound(res))
    .then((about) => about ? Object.assign(about, body).save() : null)
    .then((about) => about ? about.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  About.findById(params.id)
    .then(notFound(res))
    .then((about) => about ? about.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
