import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Subscription } from '.'

const app = () => express(apiRoot, routes)

let subscription

beforeEach(async () => {
  subscription = await Subscription.create({})
})

test('POST /subscriptions 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ email: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.email).toEqual('test')
})

test('GET /subscriptions 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /subscriptions 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /subscriptions/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${subscription.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(subscription.id)
})

test('GET /subscriptions/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${subscription.id}`)
  expect(status).toBe(401)
})

test('GET /subscriptions/:id 404 (master)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})

test('PUT /subscriptions/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${subscription.id}`)
    .send({ email: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(subscription.id)
  expect(body.email).toEqual('test')
})

test('PUT /subscriptions/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ email: 'test' })
  expect(status).toBe(404)
})

test('DELETE /subscriptions/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${subscription.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /subscriptions/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${subscription.id}`)
  expect(status).toBe(401)
})

test('DELETE /subscriptions/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
