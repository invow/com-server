import { File } from '.'
import { User } from '../user'

let user, file

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  file = await File.create({ user, name: 'test', type: 'test', path: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = file.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(file.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.name).toBe(file.name)
    expect(view.type).toBe(file.type)
    expect(view.path).toBe(file.path)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = file.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(file.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.name).toBe(file.name)
    expect(view.type).toBe(file.type)
    expect(view.path).toBe(file.path)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
