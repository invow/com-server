import request from 'supertest'
import { apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { File } from '.'

const app = () => express(apiRoot, routes)

let userSession, anotherSession, file

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const anotherUser = await User.create({ email: 'b@b.com', password: '123456' })
  userSession = signSync(user.id)
  anotherSession = signSync(anotherUser.id)
  file = await File.create({ user })
})

test('POST /files 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, name: 'test', type: 'test', path: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.type).toEqual('test')
  expect(body.path).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('POST /files 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /files 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
  expect(typeof body[0].user).toEqual('object')
})

test('GET /files 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /files/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${file.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(file.id)
  expect(typeof body.user).toEqual('object')
})

test('GET /files/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${file.id}`)
  expect(status).toBe(401)
})

test('GET /files/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /files/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${file.id}`)
    .send({ access_token: userSession, name: 'test', type: 'test', path: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(file.id)
  expect(body.name).toEqual('test')
  expect(body.type).toEqual('test')
  expect(body.path).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('PUT /files/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${file.id}`)
    .send({ access_token: anotherSession, name: 'test', type: 'test', path: 'test' })
  expect(status).toBe(401)
})

test('PUT /files/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${file.id}`)
  expect(status).toBe(401)
})

test('PUT /files/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: anotherSession, name: 'test', type: 'test', path: 'test' })
  expect(status).toBe(404)
})

test('DELETE /files/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${file.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /files/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${file.id}`)
    .send({ access_token: anotherSession })
  expect(status).toBe(401)
})

test('DELETE /files/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${file.id}`)
  expect(status).toBe(401)
})

test('DELETE /files/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: anotherSession })
  expect(status).toBe(404)
})
