import { success, notFound, authorOrAdmin } from "../../services/response/";
import { File } from ".";
import fs from "fs";

export const create = ({ user, bodymen: { body } }, res, next) =>
  File.create({ ...body, user })
    .then((file) => file.view(true))
    .then(success(res, 201))
    .catch(next);

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  File.find(query, select, cursor)
    .populate("user")
    .then((files) => files.map((file) => file.view()))
    .then(success(res))
    .catch(next);

export const show = ({ params }, res, next) =>
  File.findById(params.id)
    .populate("user")
    .then(notFound(res))
    .then((file) => (file ? file.view() : null))
    .then(success(res))
    .catch(next);

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  File.findById(params.id)
    .populate("user")
    .then(notFound(res))
    .then(authorOrAdmin(res, user, "user"))
    .then((file) => (file ? Object.assign(file, body).save() : null))
    .then((file) => (file ? file.view(true) : null))
    .then(success(res))
    .catch(next);

export const destroy = ({ user, params }, res, next) => {
  function destroyFile(file) {
    const filesUrl = __dirname.replace("src/api/file", "uploads/images");
    try {
      if (fs.existsSync(`${filesUrl}/${file}`)) {
        fs.unlink(`${filesUrl}/${file}`, (err) => {
          if (err) throw err;
        });
      }
    } catch (err) {
      throw err;
    }
  }

  File.findById(params.id)
    .then(notFound(res))
    .then(authorOrAdmin(res, user, "user"))
    .then((file) => {
      destroyFile(params.file);
      return file ? file.deleteOne() : null;
    })
    .then(success(res, 204))
    .catch(next);
};
