"use strict";
import { success, notFound } from "../../services/response/";
import { Checkout } from ".";
import { Movement } from "../movement/";
import { Operation } from "../operation/";
import { Account } from "../account/";
import { Product } from "../product/";
import { Adventure } from "../adventure/";
import { Profile } from "../profile";
import { User } from "../user";

// import { mercadopago } from 'mercadopago'

const updateContributorProfile = (data, user) => {
  console.log(user);
  Profile.findOne({ username: user.name })
    //.then(notFound(res))
    .then((profile) => {
      //Update Adventure's contributions & delivery
      let contributions = data.contributions;
      let delivery = data.delivery;
      const deliveryContent = {
        checkoutDate: new Date(),
        reward: {
          id: item._id,
          title: item.title,
        },
        contributor: user.name,
        firstName: profile.firstName,
        lastName: profile.lastName,
        address: body.address,
        phone: parseInt(body.phone),

        email: profile.email,
        invop: op.invop,
        amount: item.price,
        status: "pending",
        shipCompany: "-",
        shipTrackingCode: "-",
      };
      const contributionsContent = {
        checkoutDate: new Date(),
        reward: {
          id: item._id,
          title: item.title,
        },
        contributor: user.name,
        email: profile.email,
        invop: op.invop,
        amount: item.price,
        status: "received",
      };

      const adventureProfileContent = {
        id: data.id,
        title: data.title,
        invcode: data.invcode,
        slug: data.slug,
      };

      let profileContributions = profile.contributions;
      let profileDelivery = profile.rewards;

      switch (item.type) {
        case "reward":
          delivery.push(deliveryContent);
          data.delivery = delivery;
          data.save();
          deliveryContent.adventure = adventureProfileContent;
          deliveryContent.contributor = `${ownerFirstName} ${ownerLastName}`;
          deliveryContent.collaborators = data.collaborators;
          deliveryContent.phone = data.phone;
          profileDelivery.push(deliveryContent);
          profile.rewards = profileDelivery;
          break;
        case "contribution":
          contributions.push(contributionsContent);
          data.contributions = contributions;
          data.save();
          contributionsContent.adventure = adventureProfileContent;
          Content.profileContributions.push(
            contributionsContent
          );
          profile.contributions = profileContributions;

          break;
      }
      profile.save();
    });
}

const updateOwnerProfile = (userOwner, data, user) => {
  console.log(userOwner);
  Profile.findOne({ username: userOwner.name })
  //.then(notFound(res))
  .then((profileOwner) => {
    if(profileowner) {
      ownerFirstName = profileOwner.firstName;
      ownerLastName = profileOwner.lastName;
      ownerEmail = profileOwner.email;
      //Update profile
      updateContributorProfile(data, user);
    }
  });
}

export const create = ({ bodymen: { body }, user }, res, next) => {
  var checkout;
  var mov;
  var product;
  let op;
  var amounts = {
    ARS: 0,
    USD: 0,
  };
  try {
    var mercadopago = require("mercadopago");
    mercadopago.configure({
      access_token:
        "TEST-7234555267691506-050604-dc4b9c69fc46e2cd2ac9f76977d26381-39433492",
    });
    var oldAccessToken = mercadopago.configurations.getAccessToken();

    //Calculate percentage to divide the amount for Invow and the user

    if (body.items && body.items.length > 0) {
      body.items.forEach(function (item) {
        op = new Operation();
        op.user = user;
        op.status = "pending";
        op.total = item.price;
        op.items = JSON.stringify(body.items);
        op.currency = item.currency;
        op.save();

        if (item.type == "reward" || item.type == "contribution") {
          Adventure.findOne({ _id: item.adventure_id }, function (err, data) {
            let ownerFirstName = null;
            let ownerLastName = null;
            let ownerEmail = null;
            User.findById(data.user)
              //.then(notFound(res))
              .then((userOwner) => {
                if(userOwner) updateOwnerProfile(userOwner, data, user);
              });
            data.backers++;
            data.rewards.forEach(function (reward) {
              if (reward._id == item._id) {
                if (reward.quantity >= item.quantity) {
                  reward.quantity =
                    parseInt(reward.quantity) - parseInt(item.quantity);
                } else {
                  res.status(400).send({
                    status: 400,
                    name: "Sin stock",
                    message:
                      "No tenemos stock de lo que estas solicitando. Lo sentimos.",
                  });
                }
              }
            });
            data.save();
          });
        }

        mov = new Movement();
        mov.amount = item.price;
        mov.currency = item.currency;
        mov.from = user._id;
        mov.to = item._id;
        mov.concept = item.title;
        mov.createdAt = new Date();
        mov.updatedAt = new Date();
        mov.save();
        amounts[item.currency || "ARS"] =
          amounts[item.currency || "ARS"] + item.price;

        product = new Product();
        product.item_id = item._id;
        product.adventure_id = item.adventure_id;
        product.status = "pending";
        product.report = null;
        product.type = item.type;
        product.user = user._id;
        product.createdAt = new Date();
        product.updatedAt = new Date();
        product.save();
      });

      Account.findOne(
        { owner: user._id, currency: "ARS" },
        function (err, data) {
          if (data) {
            data.amount -= amounts.ARS;
            data.save();
            var concept =
              body.items.length == 1 ? body.items.title : "Compra en Invow";
            var payment = {
              description: concept,
              transaction_amount: parseFloat(body.amount),
              payment_method_id: body.payment.payment_method_id,
              installments: 1,
              token: body.payment.token,
              payer: {
                email: body.payment.email,
                identification: {
                  type: body.payment.docType,
                  number: body.payment.docNumber,
                },
              },
            };
            let resolved = {};
            mercadopago.payment
              .create(payment)
              .then(function (data) {
                let status = ":(";
                let name = data.body.status;
                let message = data.body.status_detail;
                if (data.body.status === "approved")
                  resolved = { invop: op.invop };
                else res.status(400).send({ status, name, message });
              })
              .catch(function (error) {
                if (error && error.name && error.status && error.message) {
                  res.status(error.status).send({
                    status: error.status,
                    name: error.name,
                    message: error.message,
                  });
                  return;
                } else {
                  resolved = error
                }
              })
              .finally(function () {
                mercadopago.configurations.setAccessToken(oldAccessToken);
                res.send(resolved)
              });
          }
        }
      );
      Account.findOne(
        { owner: user._id, currency: "USD" },
        function (err, data) {
          if (data) {
            data.amount -= amounts.USD;
            data.save();
          }
        }
      );

      //ADVENTURE ACCOUNT
      var owners = [];
      body.items.forEach(function (item) {
        if (owners.indexOf(item.adventure_id) == -1) {
          owners.push(item.adventure_id);
        }
      });
      owners.forEach(function (owner) {
        Account.findOne(
          { owner: owner, currency: "ARS" },
          function (err, data) {
            if (data) {
              body.items.forEach(function (item) {
                if (item.adventure_id == owner && item.currency != "USD") {
                  data.amount += parseInt(item.price);
                }
              });
              data.save();
            }
          }
        );
        Account.findOne(
          { owner: owner, currency: "USD" },
          function (err, data) {
            if (data) {
              body.items.forEach(function (item) {
                if (item.adventure_id == owner && item.currency == "USD")
                  data.amount += parseInt(item.amount);
              });
              data.save();
            }
          }
        );
      });
    }
    checkout = new Checkout();
    checkout.firstName = body.firstName;
    checkout.lastName = body.lastName;
    checkout.email = body.email;
    checkout.city = body.city;
    checkout.state = body.state;
    checkout.country = body.country;
    checkout.phone = body.phone;
    checkout.items = body.items;

    /**
     * @TODO >>> Agregar nuevos fields al modelo >>> revisar el origen de los datos
     */
    // checkout.invop = //
    // checkout.type = 'contribution';
    // checkout.delivery = //if type === contribution >>> delivery = 'null' : delivery = 'Pendiente de envio.' | 'En camino' | 'Entregado'
    checkout.address = body.address;

    checkout.createdAt = new Date();
    checkout.updatedAt = new Date();

    checkout.save();
  } catch (ex) {
    res.send({ err: ex });
  }
};

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Checkout.find(query, select, cursor)
    .then((checkouts) => checkouts.map((checkout) => checkout.view()))
    .then(success(res))
    .catch(next);

export const byOwner = ({ params, user }, res, next) => {
  Account.find({ owner: params.id }, function (err, data) {
    if (data) {
      res.send(data);
    }
  });
};

export const byUser = ({ params, user }, res, next) => {
  Account.find({ owner: user.id }, function (err, data) {
    if (data) {
      res.send(data);
    }
  });
};

export const update = ({ bodymen: { body }, params }, res, next) =>
  Checkout.findById(params.id)
    .then(notFound(res))
    .then((checkout) =>
      checkout ? Object.assign(checkout, body).save() : null
    )
    .then((checkout) => (checkout ? checkout.view(true) : null))
    .then(success(res))
    .catch(next);

export const destroy = ({ params }, res, next) =>
  Checkout.findById(params.id)
    .then(notFound(res))
    .then((checkout) => (checkout ? checkout.deleteOne() : null))
    .then(success(res, 204))
    .catch(next);

export const pago = ({ params }, res, next) => {
  var mercadopago = require("mercadopago");
  mercadopago.configure({
    access_token:
      "TEST-7234555267691506-050604-dc4b9c69fc46e2cd2ac9f76977d26381-39433492",
  });
  var oldAccessToken = mercadopago.configurations.getAccessToken();
  var payment = {
    description: "Buying a PS4",
    transaction_amount: body.payment.amount,
    //payment_method_id: body.payment.payment_method_id,
    payment_type_id: body.payment.payment_type_id,
    token: body.payment.token,
    payer: {
      email: body.payment.email,
      identification: {
        type: body.payment.docType,
        number: body.payment.docNumber,
      },
    },
  };
  // mercadopago.configurations.setAccessToken(config.access_token);

  mercadopago.payment
    .create(payment)
    .then(function (data) {
      res.send(data);
    })
    .catch(function (error) {})
    .finally(function () {
      mercadopago.configurations.setAccessToken(oldAccessToken);
    });
};
export const devolucion = ({ params }, res, next) => {
  var mercadopago = require("mercadopago");
  mercadopago.configure({
    access_token:
      "TEST-7234555267691506-050604-dc4b9c69fc46e2cd2ac9f76977d26381-39433492",
  });
  mercadopago.payment.refund(parseInt(params.id)).then(function (data) {
    res.send(data);
  });
};

function do_payment(params, callback) {
  var mercadopago = require("mercadopago");
  mercadopago.configure();
}
