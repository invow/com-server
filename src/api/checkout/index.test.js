import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Checkout } from '.'

const app = () => express(apiRoot, routes)

let checkout

beforeEach(async () => {
  checkout = await Checkout.create({})
})

test('POST /checkouts 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey, firstName: 'test', lastName: 'test', email: 'test', phone: 'test', address: 'test', company: 'test', country: 'test', state: 'test', city: 'test', zip: 'test', shipThisAddress: 'test', items: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.firstName).toEqual('test')
  expect(body.lastName).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.phone).toEqual('test')
  expect(body.address).toEqual('test')
  expect(body.company).toEqual('test')
  expect(body.country).toEqual('test')
  expect(body.state).toEqual('test')
  expect(body.city).toEqual('test')
  expect(body.zip).toEqual('test')
  expect(body.shipThisAddress).toEqual('test')
  expect(body.items).toEqual('test')
})

test('POST /checkouts 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /checkouts 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /checkouts/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${checkout.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(checkout.id)
})

test('GET /checkouts/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /checkouts/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${checkout.id}`)
    .send({ access_token: masterKey, firstName: 'test', lastName: 'test', email: 'test', phone: 'test', address: 'test', company: 'test', country: 'test', state: 'test', city: 'test', zip: 'test', shipThisAddress: 'test', items: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(checkout.id)
  expect(body.firstName).toEqual('test')
  expect(body.lastName).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.phone).toEqual('test')
  expect(body.address).toEqual('test')
  expect(body.company).toEqual('test')
  expect(body.country).toEqual('test')
  expect(body.state).toEqual('test')
  expect(body.city).toEqual('test')
  expect(body.zip).toEqual('test')
  expect(body.shipThisAddress).toEqual('test')
  expect(body.items).toEqual('test')
})

test('PUT /checkouts/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${checkout.id}`)
  expect(status).toBe(401)
})

test('PUT /checkouts/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, firstName: 'test', lastName: 'test', email: 'test', phone: 'test', address: 'test', company: 'test', country: 'test', state: 'test', city: 'test', zip: 'test', shipThisAddress: 'test', items: 'test' })
  expect(status).toBe(404)
})

test('DELETE /checkouts/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${checkout.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /checkouts/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${checkout.id}`)
  expect(status).toBe(401)
})

test('DELETE /checkouts/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
