import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, byUser, byOwner, update, destroy, pago, devolucion } from './controller'
import { schema } from './model'
export Checkout, { schema } from './model'

const router = new Router()
const { id, firstName, lastName, email, phone, address, company, country, state, city, zip, shipThisAddress, items, payment, amount } = schema.tree

/**
 * @api {post} /checkouts Create checkout
 * @apiName CreateCheckout
 * @apiGroup Checkout
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam firstName Checkout's firstName.
 * @apiParam lastName Checkout's lastName.
 * @apiParam email Checkout's email.
 * @apiParam phone Checkout's phone.
 * @apiParam address Checkout's address.
 * @apiParam company Checkout's company.
 * @apiParam country Checkout's country.
 * @apiParam state Checkout's state.
 * @apiParam city Checkout's city.
 * @apiParam zip Checkout's zip.
 * @apiParam shipThisAddress Checkout's shipThisAddress.
 * @apiParam items Checkout's items.
 * @apiSuccess {Object} checkout Checkout's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Checkout not found.
 * @apiError 401 master access only.
 */
router.post('/',
  token(),
  body({ firstName, lastName, email, phone, address, company, country, state, city, zip, shipThisAddress, items: [Object], payment: Object, amount }),
  create)

/**
 * @api {get} /checkouts Retrieve checkouts
 * @apiName RetrieveCheckouts
 * @apiGroup Checkout
 * @apiUse listParams
 * @apiSuccess {Object[]} checkouts List of checkouts.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /checkouts/:id Retrieve checkout
 * @apiName RetrieveCheckout
 * @apiGroup Checkout
 * @apiSuccess {Object} checkout Checkout's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Checkout not found.
 */
router.get('/byOwner/:id',
  token(),
  byOwner)

router.get('/byUser',
  token(),
  byUser)
/**
 * @api {put} /checkouts/:id Update checkout
 * @apiName UpdateCheckout
 * @apiGroup Checkout
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam firstName Checkout's firstName.
 * @apiParam lastName Checkout's lastName.
 * @apiParam email Checkout's email.
 * @apiParam phone Checkout's phone.
 * @apiParam address Checkout's address.
 * @apiParam company Checkout's company.
 * @apiParam country Checkout's country.
 * @apiParam state Checkout's state.
 * @apiParam city Checkout's city.
 * @apiParam zip Checkout's zip.
 * @apiParam shipThisAddress Checkout's shipThisAddress.
 * @apiParam items Checkout's items.
 * @apiSuccess {Object} checkout Checkout's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Checkout not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  body({ firstName, lastName, email, phone, address, company, country, state, city, zip, shipThisAddress, items: [Object] }),
  update)

router.post('/mercadopago', body({}), pago)
router.get('/refund/:id', devolucion)
/**
 * @api {delete} /checkouts/:id Delete checkout
 * @apiName DeleteCheckout
 * @apiGroup Checkout
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Checkout not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  destroy)

export default router
