import { success, notFound } from "../../services/response/";
import { Video } from ".";
import FILE from "./modules/file";

export const create = ({ bodymen: { body } }, res, next) => {
  Video.create(body)
    .then((video) => video.view(true))
    .then(success(res, 201))
    .catch(next);
};

const indexation = (query, select, cursor, res, next) => {
  Video.find(query, select, cursor)
    .then((videos) => videos.map((video) => video.view()))
    .then(success(res))
    .catch(next);
};

export const index = (
  { headers, querymen: { query, select, cursor } },
  res,
  next
) => {
  //indexation(query, select, cursor, res, next)
  FILE.get(headers, res);
};

export const show = ({ params }, res, next) =>
  Video.findById(params.id)
    .then(notFound(res))
    .then((video) => (video ? video.view() : null))
    .then(success(res))
    .catch(next);

export const update = ({ bodymen: { body }, params }, res, next) =>
  Video.findById(params.id)
    .then(notFound(res))
    .then((video) => (video ? Object.assign(video, body).save() : null))
    .then((video) => (video ? video.view(true) : null))
    .then(success(res))
    .catch(next);

export const destroy = ({ params }, res, next) =>
  Video.findById(params.id)
    .then(notFound(res))
    .then((video) => (video ? video.deleteOne() : null))
    .then(success(res, 204))
    .catch(next);
