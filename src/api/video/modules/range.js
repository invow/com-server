module.exports = {
    /**
     * @inRange
     */
    inRange: (range, path, fileSize, res, fs) => {
        if (range) {
            const parts = range.replace(/bytes=/, "").split("-")
            let start = parseInt(parts[0], 10)
            const end = parts[1]
              ? parseInt(parts[1], 10)
              : fileSize-1
        
            const chunksize = (end-start)+1    
            processRange(path, start, end, chunksize, res, fs, fileSize)
        } else {
            outRange(fs, path, fileSize, res)
        }
    }
}

/**
 * @processRange 
 */
const processRange = (path, start, end, chunksize, res, fs, fileSize) => {
    const file = fs.createReadStream(path, {start, end})
    const head = {
      'Content-Range': `bytes ${start}-${end}/${fileSize}`,
      'Accept-Ranges': 'bytes',
      'Content-Length': chunksize,
      'Content-Type': 'video/mp4',
    }
    console.log('processRange: ', head)
    res.writeHead(206, head)
    file.pipe(res)
}

const outRange = (fs, path, fileSize, res) => {
    const head = {
        'Content-Length': fileSize,
        'Content-Type': 'video/mp4',
      }
      console.log('outRange: ', head)
      res.writeHead(200, head)
      fs.createReadStream(path).pipe(res)
}