import mongoose, { Schema } from 'mongoose'

const videoSchema = new Schema({
  name: {
    type: String
  },
  file: {
    type: String
  },
  duration: {
    type: String
  },
  current: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

videoSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      file: this.file,
      duration: this.duration,
      current: this.current,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Video', videoSchema)

export const schema = model.schema
export default model
