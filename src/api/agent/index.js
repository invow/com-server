import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master, token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Agent, { schema } from './model'

const router = new Router()
const { seniority, fullName, identification, email, phone, cv, agreementAccepted } = schema.tree

/**
 * @api {post} /agents Create agent
 * @apiName CreateAgent
 * @apiGroup Agent
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam seniority Agent's seniority.
 * @apiParam fullName Agent's fullName.
 * @apiParam identification Agent's identification.
 * @apiParam email Agent's email.
 * @apiParam phone Agent's phone.
 * @apiParam cv Agent's cv.
 * @apiSuccess {Object} agent Agent's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Agent not found.
 * @apiError 401 master access only.
 */
router.post('/',
  token(),//master(),
  body({ seniority, fullName, identification, email, phone, cv, agreementAccepted }),
  create)

/**
 * @api {get} /agents Retrieve agents
 * @apiName RetrieveAgents
 * @apiGroup Agent
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} agents List of agents.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
  token(),//master(),
  query(),
  index)

/**
 * @api {get} /agents/:id Retrieve agent
 * @apiName RetrieveAgent
 * @apiGroup Agent
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} agent Agent's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Agent not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /agents/:id Update agent
 * @apiName UpdateAgent
 * @apiGroup Agent
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam seniority Agent's seniority.
 * @apiParam fullName Agent's fullName.
 * @apiParam identification Agent's identification.
 * @apiParam email Agent's email.
 * @apiParam phone Agent's phone.
 * @apiParam cv Agent's cv.
 * @apiSuccess {Object} agent Agent's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Agent not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  token(),//master(),
  body({ seniority, fullName, identification, email, phone, cv, agreementAccepted }),
  update)

/**
 * @api {delete} /agents/:id Delete agent
 * @apiName DeleteAgent
 * @apiGroup Agent
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Agent not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  token(),//master(),
  destroy)

export default router
