import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Agent } from '.'

const app = () => express(apiRoot, routes)

let userSession, adminSession, agent

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  agent = await Agent.create({})
})

test('POST /agents 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey, seniority: 'test', fullName: 'test', identification: 'test', email: 'test', phone: 'test', cv: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.seniority).toEqual('test')
  expect(body.fullName).toEqual('test')
  expect(body.identification).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.phone).toEqual('test')
  expect(body.cv).toEqual('test')
})

test('POST /agents 401 (admin)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: adminSession })
  expect(status).toBe(401)
})

test('POST /agents 401 (user)', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('POST /agents 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /agents 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /agents 401 (admin)', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: adminSession })
  expect(status).toBe(401)
})

test('GET /agents 401 (user)', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('GET /agents 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /agents/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${agent.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(agent.id)
})

test('GET /agents/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${agent.id}`)
  expect(status).toBe(401)
})

test('GET /agents/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /agents/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${agent.id}`)
    .send({ access_token: masterKey, seniority: 'test', fullName: 'test', identification: 'test', email: 'test', phone: 'test', cv: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(agent.id)
  expect(body.seniority).toEqual('test')
  expect(body.fullName).toEqual('test')
  expect(body.identification).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.phone).toEqual('test')
  expect(body.cv).toEqual('test')
})

test('PUT /agents/:id 401 (admin)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${agent.id}`)
    .send({ access_token: adminSession })
  expect(status).toBe(401)
})

test('PUT /agents/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${agent.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /agents/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${agent.id}`)
  expect(status).toBe(401)
})

test('PUT /agents/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, seniority: 'test', fullName: 'test', identification: 'test', email: 'test', phone: 'test', cv: 'test' })
  expect(status).toBe(404)
})

test('DELETE /agents/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${agent.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /agents/:id 401 (admin)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${agent.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(401)
})

test('DELETE /agents/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${agent.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /agents/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${agent.id}`)
  expect(status).toBe(401)
})

test('DELETE /agents/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
