import { Agent } from '.'

let agent

beforeEach(async () => {
  agent = await Agent.create({ seniority: 'test', fullName: 'test', identification: 'test', email: 'test', phone: 'test', cv: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = agent.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(agent.id)
    expect(view.seniority).toBe(agent.seniority)
    expect(view.fullName).toBe(agent.fullName)
    expect(view.identification).toBe(agent.identification)
    expect(view.email).toBe(agent.email)
    expect(view.phone).toBe(agent.phone)
    expect(view.cv).toBe(agent.cv)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = agent.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(agent.id)
    expect(view.seniority).toBe(agent.seniority)
    expect(view.fullName).toBe(agent.fullName)
    expect(view.identification).toBe(agent.identification)
    expect(view.email).toBe(agent.email)
    expect(view.phone).toBe(agent.phone)
    expect(view.cv).toBe(agent.cv)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
