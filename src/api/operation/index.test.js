import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Operation } from '.'

const app = () => express(apiRoot, routes)

let userSession, adminSession, operation

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  operation = await Operation.create({ user })
})

test('POST /operations 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, status: 'test', code: 'test', items: 'test', total: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.status).toEqual('test')
  expect(body.code).toEqual('test')
  expect(body.items).toEqual('test')
  expect(body.total).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('POST /operations 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /operations 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
  expect(typeof body[0].user).toEqual('object')
})

test('GET /operations 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /operations/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${operation.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(operation.id)
  expect(typeof body.user).toEqual('object')
})

test('GET /operations/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${operation.id}`)
  expect(status).toBe(401)
})

test('GET /operations/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /operations/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${operation.id}`)
    .send({ access_token: masterKey, status: 'test', code: 'test', items: 'test', total: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(operation.id)
  expect(body.status).toEqual('test')
  expect(body.code).toEqual('test')
  expect(body.items).toEqual('test')
  expect(body.total).toEqual('test')
})

test('PUT /operations/:id 401 (admin)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${operation.id}`)
    .send({ access_token: adminSession })
  expect(status).toBe(401)
})

test('PUT /operations/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${operation.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /operations/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${operation.id}`)
  expect(status).toBe(401)
})

test('PUT /operations/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, status: 'test', code: 'test', items: 'test', total: 'test' })
  expect(status).toBe(404)
})
