import { Operation } from '.'
import { User } from '../user'

let user, operation

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  operation = await Operation.create({ user, status: 'test', code: 'test', items: 'test', total: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = operation.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(operation.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.status).toBe(operation.status)
    expect(view.code).toBe(operation.code)
    expect(view.items).toBe(operation.items)
    expect(view.total).toBe(operation.total)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = operation.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(operation.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.status).toBe(operation.status)
    expect(view.code).toBe(operation.code)
    expect(view.items).toBe(operation.items)
    expect(view.total).toBe(operation.total)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
