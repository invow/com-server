import mongoose, { Schema } from 'mongoose'
var autoIncrement = require('mongoose-auto-increment')
autoIncrement.initialize(mongoose)
import mongooseKeywords from 'mongoose-keywords'

const operationSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  status: {
    type: String
  },
  invop: {
    type: Number,
    min: 1
  },
  items: {
    type: String
  },
  total: {
    type: String
  },
  currency: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

operationSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      user: this.user.view(full),
      status: this.status,
      invop: this.invop,
      items: this.items,
      total: this.total,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

operationSchema.plugin(autoIncrement.plugin, {
  model: 'Operation',
  field: 'invop',
  startAt: 1
})
operationSchema.plugin(mongooseKeywords, { paths: ['invop'] })
const model = mongoose.model('Operation', operationSchema)

export const schema = model.schema
export default model
