import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Movement, { schema } from './model'

const router = new Router()
const { from, to, amount, currency, concept, description } = schema.tree

/**
 * @api {post} /movements Create movement
 * @apiName CreateMovement
 * @apiGroup Movement
 * @apiParam from Movement's from.
 * @apiParam to Movement's to.
 * @apiParam amount Movement's amount.
 * @apiParam currency Movement's currency.
 * @apiParam concept Movement's concept.
 * @apiParam description Movement's description.
 * @apiSuccess {Object} movement Movement's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Movement not found.
 */
router.post('/',
  body({ from, to, amount, currency, concept, description }),
  create)

/**
 * @api {get} /movements Retrieve movements
 * @apiName RetrieveMovements
 * @apiGroup Movement
 * @apiUse listParams
 * @apiSuccess {Object[]} movements List of movements.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /movements/:id Retrieve movement
 * @apiName RetrieveMovement
 * @apiGroup Movement
 * @apiSuccess {Object} movement Movement's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Movement not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /movements/:id Update movement
 * @apiName UpdateMovement
 * @apiGroup Movement
 * @apiParam from Movement's from.
 * @apiParam to Movement's to.
 * @apiParam amount Movement's amount.
 * @apiParam currency Movement's currency.
 * @apiParam concept Movement's concept.
 * @apiParam description Movement's description.
 * @apiSuccess {Object} movement Movement's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Movement not found.
 */
router.put('/:id',
  body({ from, to, amount, currency, concept, description }),
  update)

/**
 * @api {delete} /movements/:id Delete movement
 * @apiName DeleteMovement
 * @apiGroup Movement
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Movement not found.
 */
router.delete('/:id',
  destroy)

export default router
