import mongoose, { Schema } from 'mongoose'

const movementSchema = new Schema({
  from: {
    type: String
  },
  to: {
    type: String
  },
  amount: {
    type: String
  },
  currency: {
    type: String
  },
  concept: {
    type: String
  },
  description: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

movementSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      from: this.from,
      to: this.to,
      amount: this.amount,
      currency: this.currency,
      concept: this.concept,
      description: this.description,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Movement', movementSchema)

export const schema = model.schema
export default model
