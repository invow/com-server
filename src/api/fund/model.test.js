import { Fund } from '.'

let fund

beforeEach(async () => {
  fund = await Fund.create({ concept: 'test', detail: 'test', input: 'test', output: 'test', amount: 'test', user: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = fund.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(fund.id)
    expect(view.concept).toBe(fund.concept)
    expect(view.detail).toBe(fund.detail)
    expect(view.input).toBe(fund.input)
    expect(view.output).toBe(fund.output)
    expect(view.amount).toBe(fund.amount)
    expect(view.user).toBe(fund.user)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = fund.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(fund.id)
    expect(view.concept).toBe(fund.concept)
    expect(view.detail).toBe(fund.detail)
    expect(view.input).toBe(fund.input)
    expect(view.output).toBe(fund.output)
    expect(view.amount).toBe(fund.amount)
    expect(view.user).toBe(fund.user)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
