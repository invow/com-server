import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Fund, { schema } from './model'

const router = new Router()
const { concept, report, ready, type, amount, currency, user } = schema.tree

/**
 * @api {post} /funds Create fund
 * @apiName CreateFund
 * @apiGroup Fund
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam concept Fund's concept.
 * @apiParam detail Fund's detail.
 * @apiParam input Fund's input.
 * @apiParam output Fund's output.
 * @apiParam amount Fund's amount.
 * @apiParam user Fund's user.
 * @apiSuccess {Object} fund Fund's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Fund not found.
 * @apiError 401 master access only.
 */
router.post('/',
  master(),
  body({ concept, report, ready, type, amount, currency, user }),
  create)

/**
 * @api {get} /funds Retrieve funds
 * @apiName RetrieveFunds
 * @apiGroup Fund
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} funds List of funds.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
  master(),
  query(),
  index)

/**
 * @api {get} /funds/:id Retrieve fund
 * @apiName RetrieveFund
 * @apiGroup Fund
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} fund Fund's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Fund not found.
 * @apiError 401 master access only.
 */
router.get('/:id',
  master(),
  show)

/**
 * @api {put} /funds/:id Update fund
 * @apiName UpdateFund
 * @apiGroup Fund
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam concept Fund's concept.
 * @apiParam detail Fund's detail.
 * @apiParam input Fund's input.
 * @apiParam output Fund's output.
 * @apiParam amount Fund's amount.
 * @apiParam user Fund's user.
 * @apiSuccess {Object} fund Fund's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Fund not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  master(),
  body({ concept, report, ready, type, amount, currency, user }),
  update)

/**
 * @api {delete} /funds/:id Delete fund
 * @apiName DeleteFund
 * @apiGroup Fund
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Fund not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
