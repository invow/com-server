import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Agreement } from '.'

const app = () => express(apiRoot, routes)

let agreement

beforeEach(async () => {
  agreement = await Agreement.create({})
})

test('POST /agreements 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ title: 'test', amount: 'test', description: 'test', adventure: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.title).toEqual('test')
  expect(body.amount).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.adventure).toEqual('test')
})

test('GET /agreements 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /agreements/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${agreement.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(agreement.id)
})

test('GET /agreements/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /agreements/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${agreement.id}`)
    .send({ title: 'test', amount: 'test', description: 'test', adventure: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(agreement.id)
  expect(body.title).toEqual('test')
  expect(body.amount).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.adventure).toEqual('test')
})

test('PUT /agreements/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ title: 'test', amount: 'test', description: 'test', adventure: 'test' })
  expect(status).toBe(404)
})

test('DELETE /agreements/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${agreement.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /agreements/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${agreement.id}`)
  expect(status).toBe(401)
})

test('DELETE /agreements/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
