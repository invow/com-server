import mongoose, { Schema } from 'mongoose'

const agreementSchema = new Schema({
  title: {
    type: String
  },
  amount: {
    type: String
  },
  description: {
    type: String
  },
  adventure: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

agreementSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      title: this.title,
      amount: this.amount,
      description: this.description,
      adventure: this.adventure,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Agreement', agreementSchema)

export const schema = model.schema
export default model
