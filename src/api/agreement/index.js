import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Agreement, { schema } from './model'

const router = new Router()
const { title, amount, description, adventure } = schema.tree

/**
 * @api {post} /agreements Create agreement
 * @apiName CreateAgreement
 * @apiGroup Agreement
 * @apiParam title Agreement's title.
 * @apiParam amount Agreement's amount.
 * @apiParam description Agreement's description.
 * @apiParam adventure Agreement's adventure.
 * @apiSuccess {Object} agreement Agreement's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Agreement not found.
 */
router.post('/',
  body({ title, amount, description, adventure }),
  create)

/**
 * @api {get} /agreements Retrieve agreements
 * @apiName RetrieveAgreements
 * @apiGroup Agreement
 * @apiUse listParams
 * @apiSuccess {Object[]} agreements List of agreements.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /agreements/:id Retrieve agreement
 * @apiName RetrieveAgreement
 * @apiGroup Agreement
 * @apiSuccess {Object} agreement Agreement's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Agreement not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /agreements/:id Update agreement
 * @apiName UpdateAgreement
 * @apiGroup Agreement
 * @apiParam title Agreement's title.
 * @apiParam amount Agreement's amount.
 * @apiParam description Agreement's description.
 * @apiParam adventure Agreement's adventure.
 * @apiSuccess {Object} agreement Agreement's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Agreement not found.
 */
router.put('/:id',
  body({ title, amount, description, adventure }),
  update)

/**
 * @api {delete} /agreements/:id Delete agreement
 * @apiName DeleteAgreement
 * @apiGroup Agreement
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Agreement not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
