import mongoose, { Schema } from 'mongoose'

const mainVideoSchema = new Schema({
  title: {
    type: String
  },
  path: {
    type: String
  },
  user: {
    type: String
  },
  slug: {
    type: String
  },
  time: {
    type: String
  },
  timeStart: {
    type: String
  },
  dateStart: {
    type: String
  },
  link: {
    type: String
  },
  adventure: {
    type: String
  },
  price: {
    type: String
  },
  active: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

mainVideoSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      title: this.title,
      path: this.path,
      user: this.user,
      slug: this.slug,
      time: this.time,
      timeStart: this.timeStart,
      dateStart: this.dateStart,
      link: this.link,
      adventure: this.adventure,
      price: this.price,
      active: this.active,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('MainVideo', mainVideoSchema)

export const schema = model.schema
export default model
