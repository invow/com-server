import { MainVideo } from '.'

let mainVideo

beforeEach(async () => {
  mainVideo = await MainVideo.create({ title: 'test', path: 'test', user: 'test', slug: 'test', time: 'test', timeStart: 'test', dateStart: 'test', link: 'test', adventure: 'test', price: 'test', active: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = mainVideo.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(mainVideo.id)
    expect(view.title).toBe(mainVideo.title)
    expect(view.path).toBe(mainVideo.path)
    expect(view.user).toBe(mainVideo.user)
    expect(view.slug).toBe(mainVideo.slug)
    expect(view.time).toBe(mainVideo.time)
    expect(view.timeStart).toBe(mainVideo.timeStart)
    expect(view.dateStart).toBe(mainVideo.dateStart)
    expect(view.link).toBe(mainVideo.link)
    expect(view.adventure).toBe(mainVideo.adventure)
    expect(view.price).toBe(mainVideo.price)
    expect(view.active).toBe(mainVideo.active)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = mainVideo.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(mainVideo.id)
    expect(view.title).toBe(mainVideo.title)
    expect(view.path).toBe(mainVideo.path)
    expect(view.user).toBe(mainVideo.user)
    expect(view.slug).toBe(mainVideo.slug)
    expect(view.time).toBe(mainVideo.time)
    expect(view.timeStart).toBe(mainVideo.timeStart)
    expect(view.dateStart).toBe(mainVideo.dateStart)
    expect(view.link).toBe(mainVideo.link)
    expect(view.adventure).toBe(mainVideo.adventure)
    expect(view.price).toBe(mainVideo.price)
    expect(view.active).toBe(mainVideo.active)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
