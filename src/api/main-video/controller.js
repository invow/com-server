import { success, notFound } from '../../services/response/'
import { MainVideo } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  MainVideo.create(body)
    .then((mainVideo) => mainVideo.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  MainVideo.find(query, select, cursor)
    .then((mainVideos) => mainVideos.map((mainVideo) => mainVideo.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  MainVideo.findById(params.id)
    .then(notFound(res))
    .then((mainVideo) => mainVideo ? mainVideo.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  MainVideo.findById(params.id)
    .then(notFound(res))
    .then((mainVideo) => mainVideo ? Object.assign(mainVideo, body).save() : null)
    .then((mainVideo) => mainVideo ? mainVideo.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  MainVideo.findById(params.id)
    .then(notFound(res))
    .then((mainVideo) => mainVideo ? mainVideo.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
