import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export MainVideo, { schema } from './model'

const router = new Router()
const { title, path, user, slug, time, timeStart, dateStart, link, adventure, price, active } = schema.tree

/**
 * @api {post} /main-videos Create main video
 * @apiName CreateMainVideo
 * @apiGroup MainVideo
 * @apiParam title Main video's title.
 * @apiParam path Main video's path.
 * @apiParam user Main video's user.
 * @apiParam slug Main video's slug.
 * @apiParam time Main video's time.
 * @apiParam timeStart Main video's timeStart.
 * @apiParam dateStart Main video's dateStart.
 * @apiParam link Main video's link.
 * @apiParam adventure Main video's adventure.
 * @apiParam price Main video's price.
 * @apiParam active Main video's active.
 * @apiSuccess {Object} mainVideo Main video's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Main video not found.
 */
router.post('/',
  body({ title, path, user, slug, time, timeStart, dateStart, link, adventure, price, active }),
  create)

/**
 * @api {get} /main-videos Retrieve main videos
 * @apiName RetrieveMainVideos
 * @apiGroup MainVideo
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} mainVideos List of main videos.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
  master(),
  query(),
  index)

/**
 * @api {get} /main-videos/:id Retrieve main video
 * @apiName RetrieveMainVideo
 * @apiGroup MainVideo
 * @apiSuccess {Object} mainVideo Main video's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Main video not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /main-videos/:id Update main video
 * @apiName UpdateMainVideo
 * @apiGroup MainVideo
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam title Main video's title.
 * @apiParam path Main video's path.
 * @apiParam user Main video's user.
 * @apiParam slug Main video's slug.
 * @apiParam time Main video's time.
 * @apiParam timeStart Main video's timeStart.
 * @apiParam dateStart Main video's dateStart.
 * @apiParam link Main video's link.
 * @apiParam adventure Main video's adventure.
 * @apiParam price Main video's price.
 * @apiParam active Main video's active.
 * @apiSuccess {Object} mainVideo Main video's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Main video not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  master(),
  body({ title, path, user, slug, time, timeStart, dateStart, link, adventure, price, active }),
  update)

/**
 * @api {delete} /main-videos/:id Delete main video
 * @apiName DeleteMainVideo
 * @apiGroup MainVideo
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Main video not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
