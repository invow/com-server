import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Profile } from '.'

const app = () => express(apiRoot, routes)

let profile

beforeEach(async () => {
  profile = await Profile.create({})
})

test('POST /profiles 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ firstName: 'test', lastName: 'test', email: 'test', birth: 'test', address: 'test', town: 'test', zip: 'test', city: 'test', country: 'test', banks: 'test', payments: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.firstName).toEqual('test')
  expect(body.lastName).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.birth).toEqual('test')
  expect(body.address).toEqual('test')
  expect(body.town).toEqual('test')
  expect(body.zip).toEqual('test')
  expect(body.city).toEqual('test')
  expect(body.country).toEqual('test')
  expect(body.banks).toEqual('test')
  expect(body.payments).toEqual('test')
})

test('GET /profiles 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /profiles/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${profile.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(profile.id)
})

test('GET /profiles/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /profiles/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${profile.id}`)
    .send({ firstName: 'test', lastName: 'test', email: 'test', birth: 'test', address: 'test', town: 'test', zip: 'test', city: 'test', country: 'test', banks: 'test', payments: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(profile.id)
  expect(body.firstName).toEqual('test')
  expect(body.lastName).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.birth).toEqual('test')
  expect(body.address).toEqual('test')
  expect(body.town).toEqual('test')
  expect(body.zip).toEqual('test')
  expect(body.city).toEqual('test')
  expect(body.country).toEqual('test')
  expect(body.banks).toEqual('test')
  expect(body.payments).toEqual('test')
})

test('PUT /profiles/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ firstName: 'test', lastName: 'test', email: 'test', birth: 'test', address: 'test', town: 'test', zip: 'test', city: 'test', country: 'test', banks: 'test', payments: 'test' })
  expect(status).toBe(404)
})

test('DELETE /profiles/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${profile.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /profiles/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${profile.id}`)
  expect(status).toBe(401)
})

test('DELETE /profiles/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
