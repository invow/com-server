import mongoose, { Schema } from 'mongoose'

const profileSchema = new Schema({
  email: {
    type: String
  },
  firstForm: {
    type: Boolean
  },
  username: {
    type: String
  },
  isPublic: {
    type: Boolean
  },
  image: {
    type: String
  },
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  shortBio: {
    type: String
  },
  phone: {
    type: Number
  },
  amount: [{
    currency: {
      code: {
        type: String
      },
      value: {
        type: String
      }
    },
    value: {
      type: Number
    }
  }],
  publications: [{
    index: {
      type: Number
    },
    publication: {
      type: String
    },
    createdAt: {
      type: Date
    },
    updatedAt: {
      type: Date
    }
  }],
  reviews: [{
    title: String,
    email: String,
    stars: {
      type: Number,
      min: 1,
      max: 5
    },
    user: {
      type: Schema.ObjectId,
      ref: 'User'
    },
    review: String
  }],
  documents: [{
    type: {
      type: String,
      minlength: 2,
      maxlength: 12
    },
    name: String,
    size: Number,
    user: {
      type: Schema.ObjectId,
      ref: 'User'
    },
    public: Boolean
  }],
  skills: [{
    index: {
      type: Number
    },
    skill: {
      type: String
    },
    description: {
      type: String
    }
  }],
  campaigns: [{
    index: {
      type: Number
    },
    campaign: {
      type: String
    }
  }],
  adventures: [{
    type: Number
  }],
  contributions: [{
    checkoutDate: {
      type: Date
    },
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    contributor: {
      type: String
    },
    email: {
      type: String
    },
    invop: {
      type: String
    },
    amount: {
      type: Number
    },
    status: {
      type: String
    },
    adventure: {
      id: {
        type: String
      },
      title: {
        type: String
      },
      invcode: {
        type: String
      },
      slug: {
        type: String
      }
    }
  }],
  rewards: [{
    checkoutDate: {
      type: Date
    },
    collaborators: [{
      email: {
        type: String
      }
    }],
    firstName: {
      type: String
    },
    reward: {
      id: {
        type: String
      },
      reward: {
        type: String
      }
    },
    lastName: {
      type: String
    },
    contributor: {
      type: String
    },
    address: {
      type: String
    },
    phone: {
      type: Number
    },
    email: {
      type: String
    },
    invop: {
      type: String
    },
    amount: {
      type: Number
    },
    status: {
      type: String
    },
    shipCompany: {
      type: String
    },
    shipTrackingCode: {
      type: String
    },
    adventure: {
      id: {
        type: String
      },
      title: {
        type: String
      },
      invcode: {
        type: String
      },
      slug: {
        type: String
      }
    }
  }],
  delivery: [{
    checkoutDate: {
      type: Date
    },
    collaborators: [{
      email: {
        type: String
      }
    }],
    firstName: {
      type: String
    },
    reward: {
      id: {
        type: String
      },
      reward: {
        type: String
      }
    },
    lastName: {
      type: String
    },
    contributor: {
      type: String
    },
    address: {
      type: String
    },
    phone: {
      type: Number
    },
    email: {
      type: String
    },
    invop: {
      type: String
    },
    amount: {
      type: Number
    },
    status: {
      type: String
    },
    shipCompany: {
      type: String
    },
    shipTrackingCode: {
      type: String
    },
    adventure: {
      id: {
        type: String
      },
      title: {
        type: String
      },
      invcode: {
        type: String
      },
      slug: {
        type: String
      }
    }
  }],
  jobs: [{
    index: {
      type: Number
    },
    job: {
      type: String
    },
    jobType: {
      type: String
    },
    country: {
      type: String
    },
    city: {
      type: String
    }
  }],
  channels: [{
    index: {
      type: Number
    },
    name: {
      type: String
    },
    hours: {
      type: Number
    },
    transmission: {
      type: String
    },
    videos: [{
      link: {
        type: String
      }
    }],
    link: {
      type: String
    },
    category: {
      type: String
    },
    description: {
      type: String
    }
  }],
  announcements: [{
    index: {
      type: Number
    },
    title: {
      type: String
    },
    category: {
      type: String
    },
    description: {
      type: String
    },
    days: {
      type: Number
    },
    views: {
      type: Number
    }
  }],
  profile: {
    type: String
  },
  email: {
    type: String
  },
  birthday: {
    type: String
  },
  address: {
    type: String
  },
  state: {
    type: String
  },
  zip: {
    type: String
  },
  city: {
    type: String
  },
  location: {
    type: String
  },
  country: {
    type: String
  },
  banks: {
    type: String
  },
  payments: {
    type: String
  },
  scope: {
    type: String
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

profileSchema.methods = {
  view (full) {
    const view = {
      // simple view
      username: this.username,
      user: this.user,
      isPublic: this.isPublic,
      image: this.image,
      id: this.id,
      firstName: this.firstName,
      lastName: this.lastName,
      shortBio: this.shortBio,
      phone: this.phone,
      amount: this.amount,
      channels: this.channels,
      announcements: this.announcements,
      publications: this.publications,
      reviews: this.reviews,
      email: this.email,
      location: this.location,
      scope: this.scope,
      firstForm: this.firstForm,
      documents: this.documents,
      skills: this.skills,
      campaigns: this.campaigns,
      adventures: this.adventures,
      contributions: this.contributions,
      delivery: this.delivery,
      rewards: this.rewards,
      jobs: this.jobs,
      email: this.email,
      birthday: this.birthday,
      address: this.address,
      state: this.state,
      zip: this.zip,
      city: this.city,
      country: this.country,
      banks: this.banks,
      payments: this.payments,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Profile', profileSchema)

export const schema = model.schema
export default model
