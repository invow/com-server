import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master, token } from '../../services/passport'
import { create, index, show, update, destroy, byUsername } from './controller'
import { schema } from './model'
export Profile, { schema } from './model'

const router = new Router()
const {
  username,
  user,
  isPublic,
  image,
  firstName,
  lastName,
  shortBio,
  amount,
  channels,
  announcements,
  publications,
  reviews,
  documents,
  skills,
  location,
  firstForm,
  scope,
  campaigns,
  adventures,
  contributions,
  delivery,
  rewards,
  jobs,
  email,
  birthday,
  address,
  state,
  zip,
  city,
  country,
  banks,
  payments
} = schema.tree

/**
 * @api {post} /profiles Create profile
 * @apiName CreateAccount
 * @apiGroup Profile
 * @apiParam firstName Profile's firstName.
 * @apiParam lastName Profile's lastName.
 * @apiParam email Profile's email.
 * @apiParam birth Profile's birth.
 * @apiParam address Profile's address.
 * @apiParam town Profile's town.
 * @apiParam zip Profile's zip.
 * @apiParam city Profile's city.
 * @apiParam country Profile's country.
 * @apiParam banks Profile's banks.
 * @apiParam payments Profile's payments.
 * @apiSuccess {Object} profile Profile's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Profile not found.
 */
router.post('/',
  body({
    username,
    user,
    isPublic,
    image,
    firstName,
    email,
    lastName,
    shortBio,
    amount: [Object],
    channels: [Object],
    announcements: [Object],
    publications: [Object],
    reviews: [Object],
    documents: [Object],
    skills: [Object],
    campaigns: [Object],
    adventures: [Object],
    contributions: [Object],
    delivery: [Object],
    rewards: [Object],
    jobs: [Object],
    email,
    birthday,
    location,
    address,
    state,
    zip,
    city,
    country,
    banks,
    payments
  }),
  token({ required: true }),
  create)

/**
 * @api {get} /profiles Retrieve profiles
 * @apiName RetrieveAccounts
 * @apiGroup Profile
 * @apiUse listParams
 * @apiSuccess {Object[]} profiles List of profiles.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /profiles/:id Retrieve profile
 * @apiName RetrieveAccount
 * @apiGroup Profile
 * @apiSuccess {Object} profile Profile's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Profile not found.
 */
router.get('/:id',
  show)

router.get('/username/:username',
  token({ required: true }),
  byUsername)

/**
 * @api {put} /profiles/:id Update profile
 * @apiName UpdateAccount
 * @apiGroup Profile
 * @apiParam firstName Profile's firstName.
 * @apiParam lastName Profile's lastName.
 * @apiParam email Profile's email.
 * @apiParam birth Profile's birth.
 * @apiParam address Profile's address.
 * @apiParam town Profile's town.
 * @apiParam zip Profile's zip.
 * @apiParam city Profile's city.
 * @apiParam country Profile's country.
 * @apiParam banks Profile's banks.
 * @apiParam payments Profile's payments.
 * @apiSuccess {Object} profile Profile's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Profile not found.
 */
router.put('/:id',
  body({
    username,
    user,
    isPublic,
    image,
    firstName,
    lastName,
    shortBio,
    email,
    amount: [Object],
    channels: [Object],
    announcements: [Object],
    publications: [Object],
    reviews: [Object],
    documents: [Object],
    skills: [Object],
    campaigns: [Object],
    adventures: [Object],
    contributions: [Object],
    delivery: [Object],
    rewards: [Object],
    jobs: [Object],
    email,
    birthday,
    location,
    firstForm,
    scope,
    address,
    state,
    zip,
    city,
    country,
    banks,
    payments
  }),
  update)

/**
 * @api {delete} /profiles/:id Delete profile
 * @apiName DeleteAccount
 * @apiGroup Profile
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Profile not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
