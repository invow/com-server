import { Profile } from '.'

let profile

beforeEach(async () => {
  profile = await Profile.create({ firstName: 'test', lastName: 'test', email: 'test', birth: 'test', address: 'test', town: 'test', zip: 'test', city: 'test', country: 'test', banks: 'test', banks: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = profile.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(profile.id)
    expect(view.firstName).toBe(profile.firstName)
    expect(view.lastName).toBe(profile.lastName)
    expect(view.email).toBe(profile.email)
    expect(view.birth).toBe(profile.birth)
    expect(view.address).toBe(profile.address)
    expect(view.town).toBe(profile.town)
    expect(view.zip).toBe(profile.zip)
    expect(view.city).toBe(profile.city)
    expect(view.country).toBe(profile.country)
    expect(view.banks).toBe(profile.banks)
    expect(view.banks).toBe(profile.banks)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = profile.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(profile.id)
    expect(view.firstName).toBe(profile.firstName)
    expect(view.lastName).toBe(profile.lastName)
    expect(view.email).toBe(profile.email)
    expect(view.birth).toBe(profile.birth)
    expect(view.address).toBe(profile.address)
    expect(view.town).toBe(profile.town)
    expect(view.zip).toBe(profile.zip)
    expect(view.city).toBe(profile.city)
    expect(view.country).toBe(profile.country)
    expect(view.banks).toBe(profile.banks)
    expect(view.banks).toBe(profile.banks)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
