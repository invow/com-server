import { Payment } from '.'

let payment

beforeEach(async () => {
  payment = await Payment.create({ cardNumber: 'test', expiry: 'test', code: 'test', type: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = payment.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(payment.id)
    expect(view.cardNumber).toBe(payment.cardNumber)
    expect(view.expiry).toBe(payment.expiry)
    expect(view.code).toBe(payment.code)
    expect(view.type).toBe(payment.type)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = payment.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(payment.id)
    expect(view.cardNumber).toBe(payment.cardNumber)
    expect(view.expiry).toBe(payment.expiry)
    expect(view.code).toBe(payment.code)
    expect(view.type).toBe(payment.type)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
