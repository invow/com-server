import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master, token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Payment, { schema } from './model'

const router = new Router()
const {
  fullName,
  birthday,
  email,
  phone,
  address,
  city,
  country,
  zip,
  identification,
  adventure,
  user,
  accountSelected,
  paymentSourceSelected,
  bankAccounts,
  paymentSources,
  idFront,
  idBack
} = schema.tree

/**
 * @api {post} /payments Create payment
 * @apiName CreatePayment
 * @apiGroup Payment
 * @apiParam cardNumber Payment's cardNumber.
 * @apiParam expiry Payment's expiry.
 * @apiParam code Payment's code.
 * @apiParam type Payment's type.
 * @apiSuccess {Object} payment Payment's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Payment not found.
 */
router.post('/',
  token({ required: true }),
  body({
    fullName,
    birthday,
    email,
    phone,
    address,
    city,
    country,
    zip,
    identification,
    adventure,
    user,
    accountSelected,
    paymentSourceSelected,
    bankAccounts,
    paymentSources,
    idFront,
    idBack
  }),
  create)

/**
 * @api {get} /payments Retrieve payments
 * @apiName RetrievePayments
 * @apiGroup Payment
 * @apiUse listParams
 * @apiSuccess {Object[]} payments List of payments.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /payments/:id Retrieve payment
 * @apiName RetrievePayment
 * @apiGroup Payment
 * @apiSuccess {Object} payment Payment's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Payment not found.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /payments/:id Update payment
 * @apiName UpdatePayment
 * @apiGroup Payment
 * @apiParam cardNumber Payment's cardNumber.
 * @apiParam expiry Payment's expiry.
 * @apiParam code Payment's code.
 * @apiParam type Payment's type.
 * @apiSuccess {Object} payment Payment's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Payment not found.
 */
router.put('/:id',
  token({ required: true }),
  body({
    fullName,
    birthday,
    email,
    phone,
    address,
    city,
    country,
    zip,
    identification,
    adventure,
    user,
    accountSelected,
    paymentSourceSelected,
    bankAccounts,
    paymentSources,
    idFront,
    idBack
  }),
  update)

/**
 * @api {delete} /payments/:id Delete payment
 * @apiName DeletePayment
 * @apiGroup Payment
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Payment not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
