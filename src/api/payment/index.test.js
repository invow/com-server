import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Payment } from '.'

const app = () => express(apiRoot, routes)

let payment

beforeEach(async () => {
  payment = await Payment.create({})
})

test('POST /payments 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ cardNumber: 'test', expiry: 'test', code: 'test', type: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.cardNumber).toEqual('test')
  expect(body.expiry).toEqual('test')
  expect(body.code).toEqual('test')
  expect(body.type).toEqual('test')
})

test('GET /payments 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /payments/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${payment.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(payment.id)
})

test('GET /payments/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /payments/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${payment.id}`)
    .send({ cardNumber: 'test', expiry: 'test', code: 'test', type: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(payment.id)
  expect(body.cardNumber).toEqual('test')
  expect(body.expiry).toEqual('test')
  expect(body.code).toEqual('test')
  expect(body.type).toEqual('test')
})

test('PUT /payments/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ cardNumber: 'test', expiry: 'test', code: 'test', type: 'test' })
  expect(status).toBe(404)
})

test('DELETE /payments/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${payment.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /payments/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${payment.id}`)
  expect(status).toBe(401)
})

test('DELETE /payments/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
