import { Faq } from '.'

let faq

beforeEach(async () => {
  faq = await Faq.create({ title: 'test', content: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = faq.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(faq.id)
    expect(view.title).toBe(faq.title)
    expect(view.content).toBe(faq.content)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = faq.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(faq.id)
    expect(view.title).toBe(faq.title)
    expect(view.content).toBe(faq.content)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
