import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Faq } from '.'

const app = () => express(apiRoot, routes)

let faq

beforeEach(async () => {
  faq = await Faq.create({})
})

test('POST /faqs 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ title: 'test', content: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.title).toEqual('test')
  expect(body.content).toEqual('test')
})

test('GET /faqs 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /faqs/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${faq.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(faq.id)
})

test('GET /faqs/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /faqs/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${faq.id}`)
    .send({ title: 'test', content: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(faq.id)
  expect(body.title).toEqual('test')
  expect(body.content).toEqual('test')
})

test('PUT /faqs/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ title: 'test', content: 'test' })
  expect(status).toBe(404)
})

test('DELETE /faqs/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${faq.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /faqs/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${faq.id}`)
  expect(status).toBe(401)
})

test('DELETE /faqs/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
