import { success, notFound } from '../../services/response/'
import { Faq } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Faq.create(body)
    .then((faq) => faq.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Faq.find(query, select, cursor)
    .then((faqs) => faqs.map((faq) => faq.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Faq.findById(params.id)
    .then(notFound(res))
    .then((faq) => faq ? faq.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Faq.findById(params.id)
    .then(notFound(res))
    .then((faq) => faq ? Object.assign(faq, body).save() : null)
    .then((faq) => faq ? faq.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Faq.findById(params.id)
    .then(notFound(res))
    .then((faq) => faq ? faq.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
