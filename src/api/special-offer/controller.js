import { success, notFound } from '../../services/response/'
import { SpecialOffer } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  SpecialOffer.create(body)
    .then((specialOffer) => specialOffer.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  SpecialOffer.find(query, select, cursor)
    .then((specialOffers) => specialOffers.map((specialOffer) => specialOffer.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  SpecialOffer.findById(params.id)
    .then(notFound(res))
    .then((specialOffer) => specialOffer ? specialOffer.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  SpecialOffer.findById(params.id)
    .then(notFound(res))
    .then((specialOffer) => specialOffer ? Object.assign(specialOffer, body).save() : null)
    .then((specialOffer) => specialOffer ? specialOffer.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  SpecialOffer.findById(params.id)
    .then(notFound(res))
    .then((specialOffer) => specialOffer ? specialOffer.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
