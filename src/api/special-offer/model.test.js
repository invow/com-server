import { SpecialOffer } from '.'

let specialOffer

beforeEach(async () => {
  specialOffer = await SpecialOffer.create({ adventure: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = specialOffer.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(specialOffer.id)
    expect(view.adventure).toBe(specialOffer.adventure)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = specialOffer.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(specialOffer.id)
    expect(view.adventure).toBe(specialOffer.adventure)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
