import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master, token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Source, { schema } from './model'

const router = new Router()
const { cardNumber, code, expiry, user, cardType } = schema.tree

/**
 * @api {post} /sources Create source
 * @apiName CreateSource
 * @apiGroup Source
 * @apiParam cardNumber Source's cardNumber.
 * @apiParam expiry Source's expiry.
 * @apiParam code Source's code.
 * @apiParam cardType Source's cardType.
 * @apiSuccess {Object} source Source's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Source not found.
 */
router.post('/',
  token({ required: true }),
  body({ cardNumber, code, expiry, user, cardType }),
  create)

/**
 * @api {get} /sources Retrieve sources
 * @apiName RetrieveSources
 * @apiGroup Source
 * @apiUse listParams
 * @apiSuccess {Object[]} sources List of sources.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /sources/:id Retrieve source
 * @apiName RetrieveSource
 * @apiSuccess {Object} source Source's data.
 * @apiGroup Source
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Source not found.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /sources/:id Update source
 * @apiName UpdateSource
 * @apiGroup Source
 * @apiParam cardNumber Source's cardNumber.
 * @apiParam expiry Source's expiry.
 * @apiParam code Source's code.
 * @apiParam cardType Source's cardType.
 * @apiSuccess {Object} source Source's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Source not found.
 */
router.put('/:id',
  token({ required: true }),
  body({ cardNumber, code, expiry, user, cardType  }),
  update)

/**
 * @api {delete} /sources/:id Delete source
 * @apiName DeleteSource
 * @apiGroup Source
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Source not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
