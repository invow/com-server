import mongoose, { Schema } from 'mongoose'

const sourceSchema = new Schema({
  cardNumber: {
    type: String
  },
  code: {
      type: String
  },
  expiry: {
    type: String
  },
  user: {
    type: String
  },
  cardType: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

sourceSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      cardNumber: this.cardNumber,
      code: this.code,
      expiry: this.expiry,
      user: this.code,
      cardType: this.cardType,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Source', sourceSchema)

export const schema = model.schema
export default model
