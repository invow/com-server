import { success, notFound } from '../../services/response/'
import { Source } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Source.create(body)
    .then((source) => source.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Source.find(query, select, cursor)
    .then((sources) => sources.map((source) => source.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
//Aplicar lógica para sacar datos de la tarjeta en la respuesta
  Source.findById(params.id)
    .then(notFound(res))
    .then((source) => source ? source.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Source.findById(params.id)
    .then(notFound(res))
    .then((source) => source ? Object.assign(source, body).save() : null)
    .then((source) => source ? source.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Source.findById(params.id)
    .then(notFound(res))
    .then((source) => source ? source.deleteOne() : null)
    .then(success(res, 204))
    .catch(next)
