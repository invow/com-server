import { Investment } from '.'
import { User } from '../user'

let user, investment

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  investment = await Investment.create({ user, account: 'test', adventure: 'test', name: 'test', description: 'test', amount: 'test', status: 'test', goalAmount: 'test', goalCategory: 'test', goalPlace: 'test', goalMethod: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = investment.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(investment.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.account).toBe(investment.account)
    expect(view.adventure).toBe(investment.adventure)
    expect(view.name).toBe(investment.name)
    expect(view.description).toBe(investment.description)
    expect(view.amount).toBe(investment.amount)
    expect(view.status).toBe(investment.status)
    expect(view.goalAmount).toBe(investment.goalAmount)
    expect(view.goalCategory).toBe(investment.goalCategory)
    expect(view.goalPlace).toBe(investment.goalPlace)
    expect(view.goalMethod).toBe(investment.goalMethod)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = investment.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(investment.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.account).toBe(investment.account)
    expect(view.adventure).toBe(investment.adventure)
    expect(view.name).toBe(investment.name)
    expect(view.description).toBe(investment.description)
    expect(view.amount).toBe(investment.amount)
    expect(view.status).toBe(investment.status)
    expect(view.goalAmount).toBe(investment.goalAmount)
    expect(view.goalCategory).toBe(investment.goalCategory)
    expect(view.goalPlace).toBe(investment.goalPlace)
    expect(view.goalMethod).toBe(investment.goalMethod)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
