import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Investment } from '.'

const app = () => express(apiRoot, routes)

let userSession, anotherSession, adminSession, investment

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const anotherUser = await User.create({ email: 'b@b.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  anotherSession = signSync(anotherUser.id)
  adminSession = signSync(admin.id)
  investment = await Investment.create({ user })
})

test('POST /investments 201 (user)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: userSession, account: 'test', adventure: 'test', name: 'test', description: 'test', amount: 'test', status: 'test', goalAmount: 'test', goalCategory: 'test', goalPlace: 'test', goalMethod: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.account).toEqual('test')
  expect(body.adventure).toEqual('test')
  expect(body.name).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.amount).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.goalAmount).toEqual('test')
  expect(body.goalCategory).toEqual('test')
  expect(body.goalPlace).toEqual('test')
  expect(body.goalMethod).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('POST /investments 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /investments 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
  expect(typeof body[0].user).toEqual('object')
})

test('GET /investments 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /investments/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${investment.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(investment.id)
  expect(typeof body.user).toEqual('object')
})

test('GET /investments/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${investment.id}`)
  expect(status).toBe(401)
})

test('GET /investments/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /investments/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${investment.id}`)
    .send({ access_token: userSession, account: 'test', adventure: 'test', name: 'test', description: 'test', amount: 'test', status: 'test', goalAmount: 'test', goalCategory: 'test', goalPlace: 'test', goalMethod: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(investment.id)
  expect(body.account).toEqual('test')
  expect(body.adventure).toEqual('test')
  expect(body.name).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.amount).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.goalAmount).toEqual('test')
  expect(body.goalCategory).toEqual('test')
  expect(body.goalPlace).toEqual('test')
  expect(body.goalMethod).toEqual('test')
  expect(typeof body.user).toEqual('object')
})

test('PUT /investments/:id 401 (user) - another user', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${investment.id}`)
    .send({ access_token: anotherSession, account: 'test', adventure: 'test', name: 'test', description: 'test', amount: 'test', status: 'test', goalAmount: 'test', goalCategory: 'test', goalPlace: 'test', goalMethod: 'test' })
  expect(status).toBe(401)
})

test('PUT /investments/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${investment.id}`)
  expect(status).toBe(401)
})

test('PUT /investments/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: anotherSession, account: 'test', adventure: 'test', name: 'test', description: 'test', amount: 'test', status: 'test', goalAmount: 'test', goalCategory: 'test', goalPlace: 'test', goalMethod: 'test' })
  expect(status).toBe(404)
})

test('DELETE /investments/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${investment.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /investments/:id 401 (admin)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${investment.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(401)
})

test('DELETE /investments/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${investment.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /investments/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${investment.id}`)
  expect(status).toBe(401)
})

test('DELETE /investments/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
