import mongoose, { Schema } from 'mongoose'

const investmentSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  account: {
    type: String
  },
  adventure: {
    type: String
  },
  name: {
    type: String
  },
  description: {
    type: String
  },
  amount: {
    type: Number
  },
  status: {
    type: String
  },
  goalAmount: {
    type: Number
  },
  goalCategory: {
    type: String
  },
  goalPlace: {
    type: String
  },
  goalMethod: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

investmentSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      user: this.user.view(full),
      account: this.account,
      adventure: this.adventure,
      name: this.name,
      description: this.description,
      amount: this.amount,
      status: this.status,
      goalAmount: this.goalAmount,
      goalCategory: this.goalCategory,
      goalPlace: this.goalPlace,
      goalMethod: this.goalMethod,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Investment', investmentSchema)

export const schema = model.schema
export default model
