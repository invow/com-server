import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Price } from '.'

const app = () => express(apiRoot, routes)

let price

beforeEach(async () => {
  price = await Price.create({})
})

test('POST /prices 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey, type: 'test', value: 'test', user: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.type).toEqual('test')
  expect(body.value).toEqual('test')
  expect(body.user).toEqual('test')
})

test('POST /prices 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /prices 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /prices 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /prices/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${price.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(price.id)
})

test('GET /prices/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /prices/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${price.id}`)
    .send({ access_token: masterKey, type: 'test', value: 'test', user: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(price.id)
  expect(body.type).toEqual('test')
  expect(body.value).toEqual('test')
  expect(body.user).toEqual('test')
})

test('PUT /prices/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${price.id}`)
  expect(status).toBe(401)
})

test('PUT /prices/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, type: 'test', value: 'test', user: 'test' })
  expect(status).toBe(404)
})
