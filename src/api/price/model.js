import mongoose, { Schema } from 'mongoose'

const priceSchema = new Schema({
  product: {
    type: String
  },
  type: {
    type: String
  },
  value: {
    type: Number
  },
  discount: {
    type: Number
  },
  includes: [{
    type: String
  }],
  user: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

priceSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      product: this.product,
      type: this.type,
      value: this.value,
      discount: this.discount,
      includes: this.includes,
      user: this.user,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Price', priceSchema)

export const schema = model.schema
export default model
