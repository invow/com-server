import mongoose, { Schema } from "mongoose";
import mongooseKeywords from "mongoose-keywords";
import searchable from "mongoose-searchable";

const resultSchema = new Schema(
  {
    name: {
      type: String,
    },
    slug: {
      type: String,
    },
    image: {
      type: String,
    },
    blurb: {
      type: String,
    },
    ready: {
      type: Boolean,
    },
    days: {
      type: String,
    },
    invcode: {
      type: Number,
    },
  },
  {
    timestamps: true,
    toJSON: {
      virtuals: true,
      transform: (obj, ret) => {
        delete ret._id;
      },
    },
  }
);

resultSchema.methods = {
  view(full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      slug: this.slug,
      image: this.image,
      blurb: this.blurb,
      ready: this.ready,
      days: this.days,
      invcode: this.invcode,
    };

    return full
      ? {
          ...view,
          // add properties for a full view
        }
      : view;
  },
};

const model = mongoose.model("Result", resultSchema);

export const schema = model.schema;
export default model;
