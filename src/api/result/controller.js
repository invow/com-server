import { success, notFound, authorOrAdmin } from "../../services/response/";
import { Result } from ".";
import fs from "fs";

export const create = ({ user, bodymen: { body } }, res, next) =>
  Result.create({ ...body, user })
    .then((result) => result.view(true))
    .then(success(res, 201))
    .catch(next);

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Result.find(query, select, cursor)
    .populate("user")
    .then((results) => results.map((result) => result.view()))
    .then(success(res))
    .catch(next);

export const show = ({ params }, res, next) =>
  Result.findById(params.id)
    .populate("user")
    .then(notFound(res))
    .then((result) => (result ? result.view() : null))
    .then(success(res))
    .catch(next);

export const update = ({ user, bodymen: { body }, params }, res, next) =>
  Result.findById(params.id)
    .populate("user")
    .then(notFound(res))
    .then(authorOrAdmin(res, user, "user"))
    .then((result) => (result ? Object.assign(result, body).save() : null))
    .then((result) => (result ? result.view(true) : null))
    .then(success(res))
    .catch(next);

export const destroy = ({ user, params }, res, next) =>
  Result.findById(params.id)
    .then(notFound(res))
    .then(authorOrAdmin(res, user, "user"))
    .then((result) => (result ? result.deleteOne() : null))
    .then(success(res, 204))
    .catch(next);
