import { Result } from '.'
import { User } from '../user'

let user, result

beforeEach(async () => {
  user = await User.create({ email: 'a@a.com', password: '123456' })
  result = await Result.create({ user, title: 'test', link: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = result.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(result.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.title).toBe(result.title)
    expect(view.link).toBe(result.link)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = result.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(result.id)
    expect(typeof view.user).toBe('object')
    expect(view.user.id).toBe(user.id)
    expect(view.title).toBe(result.title)
    expect(view.link).toBe(result.link)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
