import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Bank, { schema } from './model'

const router = new Router()
const { ownerName, ownerId, accountNumber,  user } = schema.tree

/**
 * @api {post} /banks Create bank
 * @apiName CreateBank
 * @apiGroup Bank
 * @apiParam firstName Bank's firstName.
 * @apiParam lastName Bank's lastName.
 * @apiParam identification Bank's identification.
 * @apiParam accountNumber Bank's accountNumber.
 * @apiParam sortCode Bank's sortCode.
 * @apiParam user Bank's user.
 * @apiSuccess {Object} bank Bank's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bank not found.
 */
router.post('/',
  body({ ownerName, ownerId, accountNumber, user }),
  create)

/**
 * @api {get} /banks Retrieve banks
 * @apiName RetrieveBanks
 * @apiGroup Bank
 * @apiUse listParams
 * @apiSuccess {Object[]} banks List of banks.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /banks/:id Retrieve bank
 * @apiName RetrieveBank
 * @apiGroup Bank
 * @apiSuccess {Object} bank Bank's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bank not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /banks/:id Update bank
 * @apiName UpdateBank
 * @apiGroup Bank
 * @apiParam firstName Bank's firstName.
 * @apiParam lastName Bank's lastName.
 * @apiParam identification Bank's identification.
 * @apiParam accountNumber Bank's accountNumber.
 * @apiParam sortCode Bank's sortCode.
 * @apiParam user Bank's user.
 * @apiSuccess {Object} bank Bank's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bank not found.
 */
router.put('/:id',
  body({ ownerName, ownerId, accountNumber, user }),
  update)

/**
 * @api {delete} /banks/:id Delete bank
 * @apiName DeleteBank
 * @apiGroup Bank
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Bank not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
