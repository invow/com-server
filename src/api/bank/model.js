import mongoose, { Schema } from 'mongoose'

const bankSchema = new Schema({
  ownerName: {
    type: String
  },
  ownerId: {
    type: Number
  },
  accountNumber: {
    type: Number
  },
  user: {
      type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

bankSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      ownerName: this.ownerName,
      ownerId: this.ownerId,
      accountNumber: this.accountNumber,
      user: this.user,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Bank', bankSchema)

export const schema = model.schema
export default model
