import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Bank } from '.'

const app = () => express(apiRoot, routes)

let bank

beforeEach(async () => {
  bank = await Bank.create({})
})

test('POST /banks 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ firstName: 'test', lastName: 'test', identification: 'test', accountNumber: 'test', sortCode: 'test', user: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.firstName).toEqual('test')
  expect(body.lastName).toEqual('test')
  expect(body.identification).toEqual('test')
  expect(body.accountNumber).toEqual('test')
  expect(body.sortCode).toEqual('test')
  expect(body.user).toEqual('test')
})

test('GET /banks 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /banks/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${bank.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(bank.id)
})

test('GET /banks/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /banks/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${bank.id}`)
    .send({ firstName: 'test', lastName: 'test', identification: 'test', accountNumber: 'test', sortCode: 'test', user: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(bank.id)
  expect(body.firstName).toEqual('test')
  expect(body.lastName).toEqual('test')
  expect(body.identification).toEqual('test')
  expect(body.accountNumber).toEqual('test')
  expect(body.sortCode).toEqual('test')
  expect(body.user).toEqual('test')
})

test('PUT /banks/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ firstName: 'test', lastName: 'test', identification: 'test', accountNumber: 'test', sortCode: 'test', user: 'test' })
  expect(status).toBe(404)
})

test('DELETE /banks/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${bank.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /banks/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${bank.id}`)
  expect(status).toBe(401)
})

test('DELETE /banks/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
