import { Bank } from '.'

let bank

beforeEach(async () => {
  bank = await Bank.create({ firstName: 'test', lastName: 'test', identification: 'test', accountNumber: 'test', sortCode: 'test', user: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = bank.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(bank.id)
    expect(view.firstName).toBe(bank.firstName)
    expect(view.lastName).toBe(bank.lastName)
    expect(view.identification).toBe(bank.identification)
    expect(view.accountNumber).toBe(bank.accountNumber)
    expect(view.sortCode).toBe(bank.sortCode)
    expect(view.user).toBe(bank.user)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = bank.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(bank.id)
    expect(view.firstName).toBe(bank.firstName)
    expect(view.lastName).toBe(bank.lastName)
    expect(view.identification).toBe(bank.identification)
    expect(view.accountNumber).toBe(bank.accountNumber)
    expect(view.sortCode).toBe(bank.sortCode)
    expect(view.user).toBe(bank.user)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
