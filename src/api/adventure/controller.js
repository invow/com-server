import { success, notFound } from "../../services/response/";
import { Adventure } from ".";
import { Account } from "../account/";
import { Result } from "../result";
import { Profile } from "../profile";
import { stringToSlug, addDays } from "./utils";
import { getImageSource, deleteBlob } from "./blob-storage";

import fs from "fs";

export const create = ({ bodymen: { body }, user }, res, next) => {
  body.user = user;
  body.slug = stringToSlug(body.title);
  body.contributions = [];
  body.delivery = [];

  if (body.adventureType === "impulse") {
    body.backers = 0;
    body.capital = 0;

    body.days = body.days ? body.days : 60;
    body.endDate = addDays(new Date(), body.days);

    if (body.videoLink.includes("https://vimeo")) {
      body.embed = `https://player.vimeo.com/video/${body.videoLink.substr(
        18
      )}`;
    } else {
      const idGetter = require("youtube-link-to-id");
      const ids = idGetter.linkStringToIds(body.videoLink);
      body.embed = `https://www.youtube.com/embed/${ids[0]}`;
    }
  }

  var currencies = ["ARS", "USD"];

  Adventure.create(body)
    .then((adventure) => {
      let result = {};
      result = new Result();
      result.invcode = adventure.invcode;
      result.name = adventure.title;
      result.slug = adventure.slug;
      result.image = adventure.img;
      result.blurb = adventure.blurb;
      result.days = adventure.days;
      result.save();

      Profile.findOne({ username: user.name })
        .then(notFound(res))
        .then((profile) => {
          profile.adventures.push(adventure.invcode);
          profile.save();
        });
      _recursiveAccountCreation(0, currencies, adventure._id);

      return adventure.view(true);
    })
    .then(success(res, 201))
    .catch(next);
};

function _recursiveAccountCreation(index, currencies, owner, callback) {
  if (currencies.length) {
    var newAccount = new Account();
    newAccount.owner = owner;
    newAccount.currency = currencies[index];
    newAccount.amount = 0;
    newAccount.save(function (err, data) {
      if (err) {
        throw err;
      } else {
        if (data) {
          index++;
          if (index < currencies.length)
            _recursiveAccountCreation(index, currencies, owner, callback);
          else callback && callback(data);
        }
      }
    });
  }
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Adventure.find(query, select, cursor)
    .then((adventures) =>
      adventures.map((adventure, index) => {
        const filesUrl = __dirname.replace(
          "src/api/adventure",
          "uploads/images"
        );
        try {
          if (adventure.img) adventure.img = getImageSource(adventure.img);
        } catch (err) {
          throw err;
        }
        return adventure.view();
      })
    )
    .then(success(res))
    .catch(next);

export const byInvcode = ({ params }, res, next) =>
  Adventure.findOne({ invcode: params.invcode })
    .then(notFound(res))
    .then((adventure) => (adventure ? adventure.view() : null))
    .then(success(res))
    .catch(next);

export const byType = ({ params }, res, next) =>
  Adventure.findOne({ adventureType: params.adventureType })
    .then(notFound(res))
    .then((adventure) => (adventure ? adventure.view() : null))
    .then(success(res))
    .catch(next);

export const show = ({ params }, res, next) =>
  Adventure.findById(params.id)
    .then(notFound(res))
    .then((adventure) => (adventure ? adventure.view() : null))
    .then(success(res))
    .catch(next);

export const update = ({ bodymen: { body }, params }, res, next) => {
  const newDelivery = body.delivery ? body.delivery : null;
  //chequear si el delivery es distinto >>> profile.findone > actualizar las rewards
  function clean(obj) {
    for (var propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj;
  }

  Adventure.findById(params.id)
    .then(notFound(res))
    .then((adventure) => {
      if (
        newDelivery &&
        JSON.stringify(adventure.delivery) !== JSON.stringify(newDelivery)
      ) {
        adventure.delivery.forEach((item) => {
          newDelivery.forEach((newItem) => {
            if (item.invop === newItem.invop) {
              Profile.findOne({ username: item.contributor })
                .then(notFound(res))
                .then((profile) => {
                  profile.rewards.forEach((reward, index) => {
                    if (reward.invop === item.invop) {
                      profile.rewards[index].shipCompany = newItem.shipCompany;
                      profile.rewards[index].shipTrackingCode =
                        newItem.shipTrackingCode;
                      profile.rewards[index].status = newItem.status;
                    }
                  });
                  profile.save();
                  return profile;
                });
            }
          });
        });
      }
      return adventure ? Object.assign(adventure, clean(body)).save() : null;
    })
    .then((adventure) => (adventure ? adventure.view(true) : null))
    .then(success(res))
    .catch(next);
};

export const destroy = ({ params }, res, next) =>
  Adventure.findById(params.id)
    .then(notFound(res))
    .then((adventure) => (adventure ? adventure.deleteOne() : null))
    .then(success(res, 204))
    .catch(next);

export const upload = (req, res, next) => {
  const blobName = req.files[0].blobName;
  res.json({ blobName });
};

export const deleteImage = (req, res, next) => {
  const image = req.params.image;
  deleteBlob(image);
  //res.json({ message: "Image deleted" });
};
