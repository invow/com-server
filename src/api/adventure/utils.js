import path from "path";

/**
 * Convert Title to Slug
 * @param {string} str
 */
const stringToSlug = (str) => {
  str = str.replace(/^\s+|\s+$/g, ""); // trim
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to = "aaaaeeeeiiiioooouuuunc------";
  for (var i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }

  str = str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-"); // collapse dashes

  return str;
};

/**
 * Add days to createdAt
 * @param {Date} date
 * @param {Numbers} days
 */
const addDays = (date, days) => {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
};

/**
 * Resize @crop
 * @param {*} err
 * @param {*} stdout
 * @param {*} stderr
 */
const resize = (err, stdout, stderr) => {
  if (err) console.log(err);
  if (stdout) console.log(stdout);
  if (stderr) console.log(stderr);
  console.log("resized ");
};

/**
 * Generate Thumbnail
 * @w320 @h240
 */
const generateThumbnail = () => {
  const im = require("imagemagick");

  im.crop(
    {
      srcPath: `./uploads/images/${completeName}`,
      dstPath: `./uploads/images/thumb/${completeName}`,
      width: 400,
      height: 400,
      quality: 1,
      gravity: "Center",
    },
    resize
  );

  const thumb = require("node-thumbnail").thumb;

  thumb(
    {
      source: `./uploads/images/thumb/${body.img}`, // could be a filename: dest/path/image.jpg
      destination: `./uploads/images/thumb/`,
      concurrency: 4,
    },
    (files, err, stdout, stderr) => {
      console.log(`Thumb for ${body.img} done!`);
    }
  );
};

export { stringToSlug, addDays };
