const multer = require("multer");
import { MulterAzureStorage } from "multer-azure-blob-storage";

const {
  BlobServiceClient,
  ContainerClient,
  StorageSharedKeyCredential,
} = require("@azure/storage-blob");

const containerName = process.env.CONTAINER;
const account = process.env.ACCOUNT;
const accountKey = process.env.ACCOUNT_KEY;
const connectionString = process.env.CONNECTION_STRING;

const generateImageName = (req, file) => {
  const fileType = file.mimetype.split("/")[1];
  return `adv-${new Date().getTime()}.${fileType}`;
};

const resolveBlobName = (req, file) => {
  return new Promise((resolve, reject) => {
    const blobName = generateImageName(req, file);
    resolve(blobName);
  });
};

const resolveMetadata = (req, file) => {
  return new Promise(async (resolve, reject) => {
    const metadata = file;
    resolve(metadata);
  });
};

const azureStorage = new MulterAzureStorage({
  connectionString: connectionString,
  accessKey: accountKey,
  accountName: account,
  containerName: containerName,
  blobName: resolveBlobName,
  metadata: resolveMetadata,
  containerAccessLevel: "blob",
  urlExpirationTime: 60,
});

const uploadStorage = multer({
  storage: azureStorage,
});

const getImageUrl = (req, res, next) => {
  const { name } = req.params;
  const response = getImageSource(name);
  res.json({
    src: response,
  });
};

const getImageSource = (name) => {
  return `https://${account}.blob.core.windows.net/${containerName}/${name}`;
};

async function streamToString(readableStream) {
  return new Promise((resolve, reject) => {
    const chunks = [];
    readableStream.on("data", (data) => {
      chunks.push(data.toString());
    });
    readableStream.on("end", () => {
      resolve(chunks.join(""));
    });
    readableStream.on("error", reject);
  });
}

const deleteBlob = async (file) => {
  const blobServiceClient = await BlobServiceClient.fromConnectionString(
    connectionString
  );
  const containerClient = await blobServiceClient.getContainerClient(
    containerName
  );
  const blockBlobClient = containerClient.getBlockBlobClient(file);
  const downloadBlockBlobResponse = await blockBlobClient.download(0);
  console.log(
    await streamToString(downloadBlockBlobResponse.readableStreamBody)
  );
  const blobDeleteResponse = blockBlobClient.delete();
  console.log((await blobDeleteResponse).clientRequestId);
};

export { uploadStorage, getImageUrl, getImageSource, deleteBlob };
