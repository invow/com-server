import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Adventure } from '.'

const app = () => express(apiRoot, routes)

let adventure

beforeEach(async () => {
  adventure = await Adventure.create({})
})

test('POST /adventures 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ title: 'test', capital: 'test', minimal: 'test', img: 'test', video: 'test', description: 'test', category: 'test', location: 'test', days: 'test', goal: 'test', collaborators: 'test', followers: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.title).toEqual('test')
  expect(body.capital).toEqual('test')
  expect(body.minimal).toEqual('test')
  expect(body.img).toEqual('test')
  expect(body.video).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.category).toEqual('test')
  expect(body.location).toEqual('test')
  expect(body.days).toEqual('test')
  expect(body.goal).toEqual('test')
  expect(body.collaborators).toEqual('test')
  expect(body.followers).toEqual('test')
})

test('GET /adventures 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /adventures/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${adventure.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(adventure.id)
})

test('GET /adventures/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /adventures/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${adventure.id}`)
    .send({ title: 'test', capital: 'test', minimal: 'test', img: 'test', video: 'test', description: 'test', category: 'test', location: 'test', days: 'test', goal: 'test', collaborators: 'test', followers: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(adventure.id)
  expect(body.title).toEqual('test')
  expect(body.capital).toEqual('test')
  expect(body.minimal).toEqual('test')
  expect(body.img).toEqual('test')
  expect(body.video).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.category).toEqual('test')
  expect(body.location).toEqual('test')
  expect(body.days).toEqual('test')
  expect(body.goal).toEqual('test')
  expect(body.collaborators).toEqual('test')
  expect(body.followers).toEqual('test')
})

test('PUT /adventures/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ title: 'test', capital: 'test', minimal: 'test', img: 'test', video: 'test', description: 'test', category: 'test', location: 'test', days: 'test', goal: 'test', collaborators: 'test', followers: 'test' })
  expect(status).toBe(404)
})

test('DELETE /adventures/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${adventure.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /adventures/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${adventure.id}`)
  expect(status).toBe(401)
})

test('DELETE /adventures/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
