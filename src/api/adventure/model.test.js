import { Adventure } from '.'

let adventure

beforeEach(async () => {
  adventure = await Adventure.create({ title: 'test', capital: 'test', minimal: 'test', img: 'test', video: 'test', description: 'test', category: 'test', location: 'test', days: 'test', goal: 'test', collaborators: 'test', followers: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = adventure.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(adventure.id)
    expect(view.title).toBe(adventure.title)
    expect(view.capital).toBe(adventure.capital)
    expect(view.minimal).toBe(adventure.minimal)
    expect(view.img).toBe(adventure.img)
    expect(view.video).toBe(adventure.video)
    expect(view.description).toBe(adventure.description)
    expect(view.category).toBe(adventure.category)
    expect(view.location).toBe(adventure.location)
    expect(view.days).toBe(adventure.days)i7
    expect(view.goal).toBe(adventure.goal)
    expect(view.collaborators).toBe(adventure.collaborators)
    expect(view.followers).toBe(adventure.followers)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = adventure.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(adventure.id)
    expect(view.title).toBe(adventure.title)
    expect(view.capital).toBe(adventure.capital)
    expect(view.minimal).toBe(adventure.minimal)
    expect(view.img).toBe(adventure.img)
    expect(view.video).toBe(adventure.video)
    expect(view.description).toBe(adventure.description)
    expect(view.category).toBe(adventure.category)
    expect(view.location).toBe(adventure.location)
    expect(view.days).toBe(adventure.days)
    expect(view.goal).toBe(adventure.goal)
    expect(view.collaborators).toBe(adventure.collaborators)
    expect(view.followers).toBe(adventure.followers)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
