import { Router } from "express";
import { middleware as query } from "querymen";
import { middleware as body } from "bodymen";
import { master, token } from "../../services/passport";
import {
  create,
  index,
  show,
  update,
  destroy,
  upload,
  file,
  byInvcode,
  byType,
  deleteImage,
} from "./controller";
import { schema } from "./model";
export Adventure, { schema } from "./model";

import { uploadStorage, getImageUrl } from "./blob-storage";

const router = new Router();
const {
  invcode,
  title,
  name,
  backers,
  capital,
  blurb,
  step,
  minimal,
  payment,
  img,
  thumbnail,
  video,
  videoLink,
  reviews,
  documents,
  proposals,
  contributions,
  delivery,
  charts,
  embed,
  description,
  category,
  location,
  days,
  endDate,
  goal,
  percentage,
  collaborators,
  followers,
  likes,
  adventureType,
  slug,
  user,
  priv,
  funded,
  ready,
  agreements,
  credits,
  rewards,
  participations,
} = schema.tree;

/**
 * @api {post} /adventures Create adventure
 * @apiName CreateAdventure
 * @apiGroup Adventure
 * @apiParam title Adventure's title.
 * @apiParam capital Adventure's capital.
 * @apiParam step Adventure's step.
 * @apiParam minimal Adventure's minimal.
 * @apiParam img Adventure's img.
 * @apiParam video Adventure's video.
 * @apiParam description Adventure's description.
 * @apiParam category Adventure's category.
 * @apiParam location Adventure's location.
 * @apiParam days Adventure's days.
 * @apiParam goal Adventure's goal.
 * @apiParam collaborators Adventure's collaborators.
 * @apiParam followers Adventure's followers.
 * @apiSuccess {Object} adventure Adventure's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Adventure not found.
 */
router.post(
  "/",
  token({ required: true }),
  body({
    title,
    name,
    backers,
    blurb,
    capital,
    step,
    minimal,
    payment,
    img,
    thumbnail,
    video,
    videoLink,
    embed,
    description,
    category,
    location,
    days,
    endDate,
    goal,
    percentage,
    collaborators: [Object],
    followers: [Object],
    likes: [Object],
    adventureType,
    slug,
    user,
    priv,
    funded,
    ready,
    agreements: [Object],
    credits: [Object],
    rewards: [Object],
    participations: [Object],
    reviews: [Object],
    documents: [Object],
    proposals: [Object],
    contributions: [Object],
    delivery: [Object],
    charts: [Object],
  }),
  create
);

router.post("/upload", token({ required: true }), uploadStorage.any(), upload);
router.get("/image/:name", getImageUrl);
router.get("/delete/:image", token({ required: true }), deleteImage);

/**
 * @api {get} /adventures Retrieve adventures
 * @apiName RetrieveAdventures
 * @apiGroup Adventure
 * @apiUse listParams
 * @apiSuccess {Object[]} adventures List of adventures.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get("/", query(), index);

/**
 * @api {get} /adventures/:id Retrieve adventure
 * @apiName RetrieveAdventure
 * @apiGroup Adventure
 * @apiSuccess {Object} adventure Adventure's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Adventure not found.
 */
router.get("/:id", token({ required: true }), show);

router.get("/invcode/:invcode", byInvcode);
router.get("/type/:adventureType", byType);

/**
 * @api {put} /adventures/:id Update adventure
 * @apiName UpdateAdventure
 * @apiGroup Adventure
 * @apiParam title Adventure's title.
 * @apiParam capital Adventure's capital.
 * @apiParam step Adventure's step.
 * @apiParam minimal Adventure's minimal.
 * @apiParam img Adventure's img.
 * @apiParam video Adventure's video.
 * @apiParam description Adventure's description.
 * @apiParam category Adventure's category.
 * @apiParam location Adventure's location.
 * @apiParam days Adventure's days.
 * @apiParam goal Adventure's goal.
 * @apiParam collaborators Adventure's collaborators.
 * @apiParam followers Adventure's followers.
 * @apiSuccess {Object} adventure Adventure's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Adventure not found.
 */
router.put(
  "/:id",
  token({ required: true }),
  body({
    title,
    name,
    backers,
    blurb,
    capital,
    step,
    minimal,
    payment,
    img,
    thumbnail,
    video,
    videoLink,
    embed,
    description,
    category,
    location,
    days,
    endDate,
    goal,
    percentage,
    collaborators: [Object],
    followers: [Object],
    likes: [Object],
    adventureType,
    slug,
    user,
    priv,
    funded,
    ready,
    agreements: [Object],
    credits: [Object],
    rewards: [Object],
    participations: [Object],
    reviews: [Object],
    documents: [Object],
    proposals: [Object],
    contributions: [Object],
    delivery: [Object],
    charts: [Object],
  }),
  update
);

/**
 * @api {delete} /adventures/:id Delete adventure
 * @apiName DeleteAdventure
 * @apiGroup Adventure
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Adventure not found.
 * @apiError 401 master access only.
 */
router.delete("/:id", master(), destroy);

export default router;
