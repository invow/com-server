import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Account, { schema } from './model'

const router = new Router()
const { type, owner, amount, currency } = schema.tree

/**
 * @api {post} /accounts Create account
 * @apiName CreateAccount
 * @apiGroup Account
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam type Account's type.
 * @apiParam owner Account's owner.
 * @apiParam amount Account's amount.
 * @apiParam currency Account's currency.
 * @apiSuccess {Object} account Account's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Account not found.
 * @apiError 401 master access only.
 */
router.post('/',
  master(),
  body({ type, owner, amount, currency }),
  create)

/**
 * @api {get} /accounts Retrieve accounts
 * @apiName RetrieveAccounts
 * @apiGroup Account
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} accounts List of accounts.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
  master(),
  query(),
  index)

/**
 * @api {get} /accounts/:id Retrieve account
 * @apiName RetrieveAccount
 * @apiGroup Account
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} account Account's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Account not found.
 * @apiError 401 master access only.
 */
router.get('/:id',
  master(),
  show)

/**
 * @api {put} /accounts/:id Update account
 * @apiName UpdateAccount
 * @apiGroup Account
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam type Account's type.
 * @apiParam owner Account's owner.
 * @apiParam amount Account's amount.
 * @apiParam currency Account's currency.
 * @apiSuccess {Object} account Account's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Account not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  master(),
  body({ type, owner, amount, currency }),
  update)

/**
 * @api {delete} /accounts/:id Delete account
 * @apiName DeleteAccount
 * @apiGroup Account
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Account not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
